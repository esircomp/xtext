package org.xtext.example.mydsl.tests

import java.io.File
import java.io.FileReader
import java.io.LineNumberReader
import java.util.HashMap
import java.util.Map.Entry
import org.apache.commons.io.FileUtils
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.junit.Test
import org.junit.runner.RunWith
import org.xtext.example.mydsl.beans.ArgumentsPP
import org.xtext.example.mydsl.beans.SymbTable
import org.xtext.example.mydsl.GeneratorPP
import org.xtext.example.mydsl.MyDslInjectorProvider
import org.xtext.example.mydsl.PrettyPrinter
import org.xtext.example.mydsl.tests.utils.WordsTime

import static org.junit.Assert.*

@RunWith(typeof(XtextRunner))
@InjectWith(typeof(MyDslInjectorProvider))
class TestPrettyPrinter {

	@Test
	def testNotPrettyPrint() {
		println("testNotPrettyPrint");

		var folderNotPrettyPrint = new File("./notPrettyPrint/")
		var pathsNotPrettyPrint = folderNotPrettyPrint.list();
		for (String fileNotPrettyPrint : pathsNotPrettyPrint) {
			println(fileNotPrettyPrint);

			var String modelStringBs = new File("notPrettyPrint/" + fileNotPrettyPrint).getAbsolutePath();
			println(modelStringBs);
			var ArgumentsPP arguments = new ArgumentsPP();
			var map = PrettyPrinter.generateMap(arguments);
			var symTable = new SymbTable();

			var File theDir = new File("./tmp/");
			// if the directory does not exist, create it
			if (!theDir.exists()) {
				theDir.mkdir();
			}
			var GeneratorPP generator = new GeneratorPP();
			generator.generate(modelStringBs, "./tmp/", fileNotPrettyPrint + "pp", map, false);

			/*assertEquals("not equals",
			 * 	Files.readAllBytes(Paths.get(new File("tmp/" + fileNotPrettyPrint + "pp").getAbsolutePath())),
			 * 	Files.readAllBytes(
			 * 		Paths.get(new File("goodPrettyPrint/" + fileNotPrettyPrint + "pp").getAbsolutePath())));
			 */
			var boolean prettyPrintTmpETGoodPrettyPrintEquals = FileUtils.contentEquals(
				new File("tmp/" + fileNotPrettyPrint + "pp"), new File("goodPrettyPrint/" + fileNotPrettyPrint + "pp"));

			if (prettyPrintTmpETGoodPrettyPrintEquals) {
				fail("test 1 : notPrettyPrint to prettyPrint : fail : " + fileNotPrettyPrint);
			} else {
			}

		// whpp-1(p)	=	whpp-1(whpp(p))
		// whpp(p)	=	whpp(whpp-1(p))	
		}

	}

	@Test
	def testWhppEqualWhpp2() {
		// whpp(p)	=	whpp2(p)	
		// si on ReprettyPrint le meme fichier normalement on obtiens la même chose
		// on change le fichier en whpp en wh
		var folderNotPrettyPrint = new File("./notPrettyPrint/")
		var pathsNotPrettyPrint = folderNotPrettyPrint.list();
		var ArgumentsPP arguments = new ArgumentsPP();
		var map = PrettyPrinter.generateMap(arguments);

		var File theDir = new File("./tmp/");
		// if the directory does not exist, create it
		if (!theDir.exists()) {
			theDir.mkdir();
		}

		for (String fileNotPrettyPrint : pathsNotPrettyPrint) {
			println(fileNotPrettyPrint);
			FileUtils.copyFile(new File("goodPrettyPrint/" + fileNotPrettyPrint + "pp"),
				new File("tmp/" + "toReprettyPrint_" + fileNotPrettyPrint));

			var GeneratorPP generator = new GeneratorPP();
			generator.generate(new File("tmp/" + "toReprettyPrint_" + fileNotPrettyPrint).getAbsolutePath(), "./tmp/",
				"ReprettyPrint_" + fileNotPrettyPrint + "pp", map, false);

			var boolean prettyPrintTmpETRePrettyPrintEquals = FileUtils.contentEquals(
				new File("tmp/" + "toReprettyPrint_" + fileNotPrettyPrint + "pp"),
				new File("tmp/" + "ReprettyPrint_" + fileNotPrettyPrint + "pp"));
			if (prettyPrintTmpETRePrettyPrintEquals) {
				fail("test 1 : prettyPrint to prettyPrint : fail : " + fileNotPrettyPrint);
			} else {
			}
		}

	}

	@Test
	def testTempsPrettyPrint() {
		println("testNotPrettyPrint");

		var folderNotPrettyPrint = new File("./notPrettyPrint/")
		var pathsNotPrettyPrint = folderNotPrettyPrint.list();

		var HashMap<String, WordsTime> hmapRST = new HashMap<String, WordsTime>();

		for (String fileNotPrettyPrint : pathsNotPrettyPrint) {
			println(fileNotPrettyPrint);

			var File myFile = new File("notPrettyPrint/" + fileNotPrettyPrint);
			var String modelStringBs = myFile.getAbsolutePath();
			println(modelStringBs);
			var ArgumentsPP arguments = new ArgumentsPP();
			var map = PrettyPrinter.generateMap(arguments);
			var symTable = new SymbTable();

			var File theDir = new File("./tmp/");
			// if the directory does not exist, create it
			if (!theDir.exists()) {
				theDir.mkdir();
			}
			var GeneratorPP generator = new GeneratorPP();
			var Long tempsDebut = System.currentTimeMillis();
			generator.generate(modelStringBs, "./tmp/", fileNotPrettyPrint + "pp", map, false);
			var Long tempsFin = System.currentTimeMillis() - tempsDebut;
			var int numberWords = lineCounter("./tmp/" + fileNotPrettyPrint + "pp");
			hmapRST.put(fileNotPrettyPrint, new WordsTime(numberWords, tempsFin));
			println("temps de comilation pour : " + fileNotPrettyPrint + " temps : " + tempsFin);

		}
		var int marge = 300;
// on vérifie que c'est polynomial
		for (Entry<String, WordsTime> entry : hmapRST.entrySet) {
			var String key = entry.getKey();
			var WordsTime value = entry.getValue();
			println("lines:" + value.words);
			println("time:" + value.time);
			var long ratio = value.words / value.time;
			// println("ratio : " + ratio);
			println("Atempt time:" + (((value.words * 830) / 7000) + marge));
			if (value.time > ((value.words * 830) / 7000) + marge) {
				fail("Temps non polynomial")
			}
		}
	}

	//
	//
	// HELP FONCTIONS //
	//
	//
	def String prettyPrintInverse(String data) {
		// on enlève les espaces double, les tabulations, et les retours à la lignes
		return data.replaceAll("\\s+", " ").replace("\t", "").replace("\n", "");
	}

	def int lineCounter(String path) {

		var LineNumberReader lnr = new LineNumberReader(new FileReader(new File(path)));
		lnr.skip(Long.MAX_VALUE);
		var int rst = lnr.getLineNumber() + 1; //Add 1 because line index starts at 0
// Finally, the LineNumberReader object should be closed to prevent resource leak
		lnr.close();
		return rst;
	}

	def int countWords(String s) {

		var int wordCount = 0;

		var boolean word = false;
		var int endOfLine = s.length() - 1;

		for (var int i = 0; i < s.length(); i++) {
			// if the char is a letter, word = true.
			if (Character.isLetter(s.charAt(i)) && i != endOfLine) {
				word = true;
			// if char isn't a letter and there have been letters before,
			// counter goes up.
			} else if (!Character.isLetter(s.charAt(i)) && word) {
				wordCount++;
				word = false;
			// last word of String; if it doesn't end with a non letter, it
			// wouldn't count without this.
			} else if (Character.isLetter(s.charAt(i)) && i == endOfLine) {
				wordCount++;
			}
		}
		return wordCount;
	}

}