package org.xtext.example.mydsl.tests.utils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class GeneratorUtils {
	
	public static void main(String[] args) {
		creerFichierWhileSimple(10);
		creerFichierWhileSimple(100);
		creerFichierWhileSimple(1000);
		creerFichierWhileSimple(2000);
		creerFichierWhileComplexe(1);
		creerFichierWhileComplexe(10);
		creerFichierWhileComplexe(100);
		creerFichierWhileComplexe(200);
		creerFichierWhileComplexe(300);
		System.out.println("end");

	}
	private static final String mainFolder = "./notPrettyPrint/";
	private static int fileNumber=0;
	private static void creerFichierWhileSimple(int nombreFonction){
		StringBuilder st = new StringBuilder();
		fileNumber++;
		for(int i = 0; i < nombreFonction; ++i){
			st.append("function test1!:read A!%A! := A!%write A!");
			
		}

		FileWriter fw;
		try {
			fw = new FileWriter(mainFolder +"file"+String.valueOf(fileNumber) +".wh");
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(st.toString());
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private static void creerFichierWhileComplexe(int imbricationCount){
		StringBuilder st = new StringBuilder();
		st.append("function test1! : read A->A!,B!,C! %");	
		fileNumber++;
		for(int y = 0; y < imbricationCount; ++y){
			st.append("while (hd A!) do A! := B!; B! := C!; if (A!) then nop fi; ");
		}
		for(int u = 0; u < imbricationCount; ++u){
			st.append(" od; ");
		}
		st.append(" %write B! ");



		FileWriter fw;
		try {
			fw = new FileWriter(mainFolder +"file"+String.valueOf(fileNumber) +".wh");
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(st.toString());
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
