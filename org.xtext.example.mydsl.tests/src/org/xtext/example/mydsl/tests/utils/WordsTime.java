package org.xtext.example.mydsl.tests.utils;

public class WordsTime {
	private int words;
	private Long time;
	public int getWords() {
		return words;
	}
	public Long getTime() {
		return time;
	}
	public WordsTime(int words, Long time) {
		super();
		this.words = words;
		this.time = time;
	}

}
