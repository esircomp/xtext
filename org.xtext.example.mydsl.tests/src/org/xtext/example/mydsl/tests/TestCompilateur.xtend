package org.xtext.example.mydsl.tests

import java.io.File
import org.apache.commons.io.FileUtils
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.junit.Test
import org.junit.runner.RunWith
import org.xtext.example.mydsl.Compilateur
import org.xtext.example.mydsl.GeneratorPP
import org.xtext.example.mydsl.beans.*
import org.xtext.example.mydsl.MyDslInjectorProvider
import org.xtext.example.mydsl.PrettyPrinter

import static org.junit.Assert.*

@RunWith(typeof(XtextRunner))
@InjectWith(typeof(MyDslInjectorProvider))
class TestCompilateur {

	@Test
	def testCompilerProgrammeOK() {
		println("---------------------------------------Test testCompilerProgrammeOK--------------------------------------");
		println("testNotPrettyPrint");

		var folderNotPrettyPrint = new File("./testCompilateursSimple/")
		var pathsNotPrettyPrint = folderNotPrettyPrint.list();
		for (String fileNotPrettyPrint : pathsNotPrettyPrint) {
			println(fileNotPrettyPrint);

			var String modelStringBs = new File("testCompilateursSimple/" + fileNotPrettyPrint).getAbsolutePath();
			println(modelStringBs);
			var ArgumentsPP arguments = new ArgumentsPP();
			var map = PrettyPrinter.generateMap(arguments);
			var symTable = new SymbTable();

			var File theDir = new File("./tmp/");
			// if the directory does not exist, create it
			if (!theDir.exists()) {
				theDir.mkdir();
			}
			var GeneratorPP generator = new GeneratorPP();
			generator.generate(modelStringBs, "./tmp/", fileNotPrettyPrint + "pp", map, false);

			/*assertEquals("not equals",
			 * 	Files.readAllBytes(Paths.get(new File("tmp/" + fileNotPrettyPrint + "pp").getAbsolutePath())),
			 * 	Files.readAllBytes(
			 * 		Paths.get(new File("goodPrettyPrint/" + fileNotPrettyPrint + "pp").getAbsolutePath())));
			 */
			var boolean prettyPrintTmpETGoodPrettyPrintEquals = FileUtils.contentEquals(
				new File("tmp/" + fileNotPrettyPrint + "pp"), new File("goodPrettyPrint/" + fileNotPrettyPrint + "pp"));

			if (prettyPrintTmpETGoodPrettyPrintEquals) {
				fail("test 1 : notPrettyPrint to prettyPrint : fail : " + fileNotPrettyPrint);
			} else {
			}
		}

	}

	@Test
	def void testCompilateurSimple() {
		println("---------------------------------------Test testCompilateurSimple--------------------------------------");
		println("testNotPrettyPrint");
		var boolean anErrorOccure = false;
		var String marche = "";
		var String marchePas = "";
		var folderSimpleProg = new File("./testCompilateursSimple/")
		var pathsSimpleProg = folderSimpleProg.list();
		for (String fileNotPrettyPrint : pathsSimpleProg) {
			var File file = new File("testCompilateursSimple/" + fileNotPrettyPrint)
			var String pathFile = file.getAbsolutePath();
			println(pathFile);
			try {
				if (!Compilateur.generateJava(pathFile)) {
					anErrorOccure = false;
					marchePas += new File(pathFile).getName();
					marchePas += System.lineSeparator();

				} else {
					marche += new File(pathFile).getName();
					marche += System.lineSeparator();
				}

			} catch (Exception e) {
				anErrorOccure = false;
				marchePas += new File(pathFile).getName();
				marchePas += System.lineSeparator();
				e.printStackTrace;
			}

		}
		if (anErrorOccure != true) {
			fail("marche : " + marche + "\n" + "marche pas: " + marchePas);
		} else {
			println("tout est OK");
		}

	}

	@Test
	def void testExecution() {
		println("---------------------------------------Test Execution--------------------------------------");
		//génération des .class
	
		println("testNotPrettyPrint");
		var boolean anErrorOccure = false;
		var folderSimpleProg = new File("./testCompilateursSimple/")
		var pathsSimpleProg = folderSimpleProg.list();
		for (String fileNotPrettyPrint : pathsSimpleProg) {
			var File file = new File("testCompilateursSimple/" + fileNotPrettyPrint)
			var String pathFile = file.getAbsolutePath();
			println(pathFile);
			
			
			//execution
		
 			var String fileName = new File(pathFile).getName();
 			if ( fileName.equals("test_if")){
 				//Génération 
			Compilateur.generateJava(new Boolean(true),pathFile);
 				var Object[] parameters = #{"true","false"};
				
 				assertEquals("[true]",Compilateur.executeJavaAndGetRst(fileName,parameters),"Mauvais retour lors de l'execution avec les parametres :"+getPrametres(parameters)+" ");

 			}
 			
						

		}
		
		

	}
	
	def String getPrametres(Object[] objects) {
		var String rst="";
		
		for(var int i=0; i<objects.size;i++){
			rst+=objects.get(i)+"|";
		}
		return rst;
	}

}