/**
 */
package org.xtext.example.mydsl.myDsl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.xtext.example.mydsl.myDsl.MyDslFactory
 * @model kind="package"
 * @generated
 */
public interface MyDslPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "myDsl";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.xtext.org/example/mydsl/MyDsl";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "myDsl";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  MyDslPackage eINSTANCE = org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl.init();

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.ModelImpl <em>Model</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.ModelImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getModel()
   * @generated
   */
  int MODEL = 0;

  /**
   * The feature id for the '<em><b>Prog</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL__PROG = 0;

  /**
   * The number of structural features of the '<em>Model</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.FunctionImpl <em>Function</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.FunctionImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getFunction()
   * @generated
   */
  int FUNCTION = 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION__NAME = 0;

  /**
   * The feature id for the '<em><b>Def</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION__DEF = 1;

  /**
   * The number of structural features of the '<em>Function</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.DefinitionImpl <em>Definition</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.DefinitionImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getDefinition()
   * @generated
   */
  int DEFINITION = 2;

  /**
   * The feature id for the '<em><b>In</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINITION__IN = 0;

  /**
   * The feature id for the '<em><b>Com</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINITION__COM = 1;

  /**
   * The feature id for the '<em><b>Out</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINITION__OUT = 2;

  /**
   * The number of structural features of the '<em>Definition</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINITION_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.InputImpl <em>Input</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.InputImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getInput()
   * @generated
   */
  int INPUT = 3;

  /**
   * The feature id for the '<em><b>V</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INPUT__V = 0;

  /**
   * The number of structural features of the '<em>Input</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INPUT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.OutputImpl <em>Output</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.OutputImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getOutput()
   * @generated
   */
  int OUTPUT = 4;

  /**
   * The feature id for the '<em><b>V</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OUTPUT__V = 0;

  /**
   * The number of structural features of the '<em>Output</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OUTPUT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.CommandsImpl <em>Commands</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.CommandsImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getCommands()
   * @generated
   */
  int COMMANDS = 5;

  /**
   * The feature id for the '<em><b>C</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMANDS__C = 0;

  /**
   * The number of structural features of the '<em>Commands</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMANDS_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.CommandImpl <em>Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.CommandImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getCommand()
   * @generated
   */
  int COMMAND = 6;

  /**
   * The feature id for the '<em><b>Nop</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMAND__NOP = 0;

  /**
   * The feature id for the '<em><b>Assign</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMAND__ASSIGN = 1;

  /**
   * The feature id for the '<em><b>Wh</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMAND__WH = 2;

  /**
   * The feature id for the '<em><b>For</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMAND__FOR = 3;

  /**
   * The feature id for the '<em><b>If</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMAND__IF = 4;

  /**
   * The feature id for the '<em><b>Fore</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMAND__FORE = 5;

  /**
   * The feature id for the '<em><b>Ifc</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMAND__IFC = 6;

  /**
   * The number of structural features of the '<em>Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMAND_FEATURE_COUNT = 7;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.AssignImpl <em>Assign</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.AssignImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getAssign()
   * @generated
   */
  int ASSIGN = 7;

  /**
   * The feature id for the '<em><b>Vs</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGN__VS = 0;

  /**
   * The feature id for the '<em><b>Ex</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGN__EX = 1;

  /**
   * The number of structural features of the '<em>Assign</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGN_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.WhileImpl <em>While</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.WhileImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getWhile()
   * @generated
   */
  int WHILE = 8;

  /**
   * The feature id for the '<em><b>Ex</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHILE__EX = 0;

  /**
   * The feature id for the '<em><b>C</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHILE__C = 1;

  /**
   * The number of structural features of the '<em>While</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHILE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.ForImpl <em>For</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.ForImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getFor()
   * @generated
   */
  int FOR = 9;

  /**
   * The feature id for the '<em><b>Ex</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOR__EX = 0;

  /**
   * The feature id for the '<em><b>C</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOR__C = 1;

  /**
   * The number of structural features of the '<em>For</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOR_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.IfImpl <em>If</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.IfImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getIf()
   * @generated
   */
  int IF = 10;

  /**
   * The feature id for the '<em><b>Ex</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF__EX = 0;

  /**
   * The feature id for the '<em><b>Ct</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF__CT = 1;

  /**
   * The feature id for the '<em><b>Ce</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF__CE = 2;

  /**
   * The number of structural features of the '<em>If</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.ForeachImpl <em>Foreach</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.ForeachImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getForeach()
   * @generated
   */
  int FOREACH = 11;

  /**
   * The feature id for the '<em><b>Ex1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOREACH__EX1 = 0;

  /**
   * The feature id for the '<em><b>Ex2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOREACH__EX2 = 1;

  /**
   * The feature id for the '<em><b>C</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOREACH__C = 2;

  /**
   * The number of structural features of the '<em>Foreach</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOREACH_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.IfconfortImpl <em>Ifconfort</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.IfconfortImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getIfconfort()
   * @generated
   */
  int IFCONFORT = 12;

  /**
   * The feature id for the '<em><b>Ex</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IFCONFORT__EX = 0;

  /**
   * The feature id for the '<em><b>C</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IFCONFORT__C = 1;

  /**
   * The number of structural features of the '<em>Ifconfort</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IFCONFORT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.VarsImpl <em>Vars</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.VarsImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getVars()
   * @generated
   */
  int VARS = 13;

  /**
   * The feature id for the '<em><b>Vs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARS__VS = 0;

  /**
   * The number of structural features of the '<em>Vars</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARS_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.ExprsImpl <em>Exprs</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.ExprsImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getExprs()
   * @generated
   */
  int EXPRS = 14;

  /**
   * The feature id for the '<em><b>Ex</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRS__EX = 0;

  /**
   * The number of structural features of the '<em>Exprs</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRS_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.ExprImpl <em>Expr</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.ExprImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getExpr()
   * @generated
   */
  int EXPR = 15;

  /**
   * The feature id for the '<em><b>Exs</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPR__EXS = 0;

  /**
   * The feature id for the '<em><b>Exa</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPR__EXA = 1;

  /**
   * The number of structural features of the '<em>Expr</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPR_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.ExprSimpleImpl <em>Expr Simple</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.ExprSimpleImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getExprSimple()
   * @generated
   */
  int EXPR_SIMPLE = 16;

  /**
   * The feature id for the '<em><b>Nil</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPR_SIMPLE__NIL = 0;

  /**
   * The feature id for the '<em><b>V</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPR_SIMPLE__V = 1;

  /**
   * The feature id for the '<em><b>Sym</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPR_SIMPLE__SYM = 2;

  /**
   * The feature id for the '<em><b>Mot</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPR_SIMPLE__MOT = 3;

  /**
   * The feature id for the '<em><b>Lex</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPR_SIMPLE__LEX = 4;

  /**
   * The feature id for the '<em><b>Ex</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPR_SIMPLE__EX = 5;

  /**
   * The number of structural features of the '<em>Expr Simple</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPR_SIMPLE_FEATURE_COUNT = 6;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.LExprImpl <em>LExpr</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.LExprImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getLExpr()
   * @generated
   */
  int LEXPR = 17;

  /**
   * The feature id for the '<em><b>E</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LEXPR__E = 0;

  /**
   * The number of structural features of the '<em>LExpr</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LEXPR_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.ExprAndImpl <em>Expr And</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.ExprAndImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getExprAnd()
   * @generated
   */
  int EXPR_AND = 18;

  /**
   * The feature id for the '<em><b>Exo1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPR_AND__EXO1 = 0;

  /**
   * The feature id for the '<em><b>Exo2</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPR_AND__EXO2 = 1;

  /**
   * The number of structural features of the '<em>Expr And</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPR_AND_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.ExprOrImpl <em>Expr Or</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.ExprOrImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getExprOr()
   * @generated
   */
  int EXPR_OR = 19;

  /**
   * The feature id for the '<em><b>Exn1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPR_OR__EXN1 = 0;

  /**
   * The feature id for the '<em><b>Exn2</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPR_OR__EXN2 = 1;

  /**
   * The number of structural features of the '<em>Expr Or</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPR_OR_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.ExprNotImpl <em>Expr Not</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.ExprNotImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getExprNot()
   * @generated
   */
  int EXPR_NOT = 20;

  /**
   * The feature id for the '<em><b>Ex Q1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPR_NOT__EX_Q1 = 0;

  /**
   * The feature id for the '<em><b>Ex Q2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPR_NOT__EX_Q2 = 1;

  /**
   * The number of structural features of the '<em>Expr Not</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPR_NOT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.ExprEqImpl <em>Expr Eq</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.ExprEqImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getExprEq()
   * @generated
   */
  int EXPR_EQ = 21;

  /**
   * The feature id for the '<em><b>Ex S1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPR_EQ__EX_S1 = 0;

  /**
   * The feature id for the '<em><b>Ex S2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPR_EQ__EX_S2 = 1;

  /**
   * The feature id for the '<em><b>Ex</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPR_EQ__EX = 2;

  /**
   * The number of structural features of the '<em>Expr Eq</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPR_EQ_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.VARImpl <em>VAR</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.VARImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getVAR()
   * @generated
   */
  int VAR = 22;

  /**
   * The feature id for the '<em><b>Bv</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR__BV = 0;

  /**
   * The feature id for the '<em><b>Cf</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR__CF = 1;

  /**
   * The number of structural features of the '<em>VAR</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.SYMBImpl <em>SYMB</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.SYMBImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getSYMB()
   * @generated
   */
  int SYMB = 23;

  /**
   * The feature id for the '<em><b>Bs</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYMB__BS = 0;

  /**
   * The feature id for the '<em><b>Cf</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYMB__CF = 1;

  /**
   * The number of structural features of the '<em>SYMB</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYMB_FEATURE_COUNT = 2;


  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.Model <em>Model</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Model</em>'.
   * @see org.xtext.example.mydsl.myDsl.Model
   * @generated
   */
  EClass getModel();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.myDsl.Model#getProg <em>Prog</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Prog</em>'.
   * @see org.xtext.example.mydsl.myDsl.Model#getProg()
   * @see #getModel()
   * @generated
   */
  EReference getModel_Prog();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.Function <em>Function</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Function</em>'.
   * @see org.xtext.example.mydsl.myDsl.Function
   * @generated
   */
  EClass getFunction();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.Function#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Name</em>'.
   * @see org.xtext.example.mydsl.myDsl.Function#getName()
   * @see #getFunction()
   * @generated
   */
  EReference getFunction_Name();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.Function#getDef <em>Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Def</em>'.
   * @see org.xtext.example.mydsl.myDsl.Function#getDef()
   * @see #getFunction()
   * @generated
   */
  EReference getFunction_Def();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.Definition <em>Definition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Definition</em>'.
   * @see org.xtext.example.mydsl.myDsl.Definition
   * @generated
   */
  EClass getDefinition();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.Definition#getIn <em>In</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>In</em>'.
   * @see org.xtext.example.mydsl.myDsl.Definition#getIn()
   * @see #getDefinition()
   * @generated
   */
  EReference getDefinition_In();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.Definition#getCom <em>Com</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Com</em>'.
   * @see org.xtext.example.mydsl.myDsl.Definition#getCom()
   * @see #getDefinition()
   * @generated
   */
  EReference getDefinition_Com();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.Definition#getOut <em>Out</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Out</em>'.
   * @see org.xtext.example.mydsl.myDsl.Definition#getOut()
   * @see #getDefinition()
   * @generated
   */
  EReference getDefinition_Out();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.Input <em>Input</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Input</em>'.
   * @see org.xtext.example.mydsl.myDsl.Input
   * @generated
   */
  EClass getInput();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.myDsl.Input#getV <em>V</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>V</em>'.
   * @see org.xtext.example.mydsl.myDsl.Input#getV()
   * @see #getInput()
   * @generated
   */
  EReference getInput_V();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.Output <em>Output</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Output</em>'.
   * @see org.xtext.example.mydsl.myDsl.Output
   * @generated
   */
  EClass getOutput();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.myDsl.Output#getV <em>V</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>V</em>'.
   * @see org.xtext.example.mydsl.myDsl.Output#getV()
   * @see #getOutput()
   * @generated
   */
  EReference getOutput_V();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.Commands <em>Commands</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Commands</em>'.
   * @see org.xtext.example.mydsl.myDsl.Commands
   * @generated
   */
  EClass getCommands();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.myDsl.Commands#getC <em>C</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>C</em>'.
   * @see org.xtext.example.mydsl.myDsl.Commands#getC()
   * @see #getCommands()
   * @generated
   */
  EReference getCommands_C();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.Command <em>Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Command</em>'.
   * @see org.xtext.example.mydsl.myDsl.Command
   * @generated
   */
  EClass getCommand();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDsl.Command#getNop <em>Nop</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Nop</em>'.
   * @see org.xtext.example.mydsl.myDsl.Command#getNop()
   * @see #getCommand()
   * @generated
   */
  EAttribute getCommand_Nop();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.Command#getAssign <em>Assign</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Assign</em>'.
   * @see org.xtext.example.mydsl.myDsl.Command#getAssign()
   * @see #getCommand()
   * @generated
   */
  EReference getCommand_Assign();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.Command#getWh <em>Wh</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Wh</em>'.
   * @see org.xtext.example.mydsl.myDsl.Command#getWh()
   * @see #getCommand()
   * @generated
   */
  EReference getCommand_Wh();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.Command#getFor <em>For</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>For</em>'.
   * @see org.xtext.example.mydsl.myDsl.Command#getFor()
   * @see #getCommand()
   * @generated
   */
  EReference getCommand_For();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.Command#getIf <em>If</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>If</em>'.
   * @see org.xtext.example.mydsl.myDsl.Command#getIf()
   * @see #getCommand()
   * @generated
   */
  EReference getCommand_If();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.Command#getFore <em>Fore</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Fore</em>'.
   * @see org.xtext.example.mydsl.myDsl.Command#getFore()
   * @see #getCommand()
   * @generated
   */
  EReference getCommand_Fore();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.Command#getIfc <em>Ifc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ifc</em>'.
   * @see org.xtext.example.mydsl.myDsl.Command#getIfc()
   * @see #getCommand()
   * @generated
   */
  EReference getCommand_Ifc();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.Assign <em>Assign</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Assign</em>'.
   * @see org.xtext.example.mydsl.myDsl.Assign
   * @generated
   */
  EClass getAssign();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.Assign#getVs <em>Vs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Vs</em>'.
   * @see org.xtext.example.mydsl.myDsl.Assign#getVs()
   * @see #getAssign()
   * @generated
   */
  EReference getAssign_Vs();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.Assign#getEx <em>Ex</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ex</em>'.
   * @see org.xtext.example.mydsl.myDsl.Assign#getEx()
   * @see #getAssign()
   * @generated
   */
  EReference getAssign_Ex();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.While <em>While</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>While</em>'.
   * @see org.xtext.example.mydsl.myDsl.While
   * @generated
   */
  EClass getWhile();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.While#getEx <em>Ex</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ex</em>'.
   * @see org.xtext.example.mydsl.myDsl.While#getEx()
   * @see #getWhile()
   * @generated
   */
  EReference getWhile_Ex();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.While#getC <em>C</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>C</em>'.
   * @see org.xtext.example.mydsl.myDsl.While#getC()
   * @see #getWhile()
   * @generated
   */
  EReference getWhile_C();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.For <em>For</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>For</em>'.
   * @see org.xtext.example.mydsl.myDsl.For
   * @generated
   */
  EClass getFor();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.For#getEx <em>Ex</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ex</em>'.
   * @see org.xtext.example.mydsl.myDsl.For#getEx()
   * @see #getFor()
   * @generated
   */
  EReference getFor_Ex();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.For#getC <em>C</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>C</em>'.
   * @see org.xtext.example.mydsl.myDsl.For#getC()
   * @see #getFor()
   * @generated
   */
  EReference getFor_C();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.If <em>If</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>If</em>'.
   * @see org.xtext.example.mydsl.myDsl.If
   * @generated
   */
  EClass getIf();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.If#getEx <em>Ex</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ex</em>'.
   * @see org.xtext.example.mydsl.myDsl.If#getEx()
   * @see #getIf()
   * @generated
   */
  EReference getIf_Ex();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.If#getCt <em>Ct</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ct</em>'.
   * @see org.xtext.example.mydsl.myDsl.If#getCt()
   * @see #getIf()
   * @generated
   */
  EReference getIf_Ct();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.If#getCe <em>Ce</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ce</em>'.
   * @see org.xtext.example.mydsl.myDsl.If#getCe()
   * @see #getIf()
   * @generated
   */
  EReference getIf_Ce();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.Foreach <em>Foreach</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Foreach</em>'.
   * @see org.xtext.example.mydsl.myDsl.Foreach
   * @generated
   */
  EClass getForeach();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.Foreach#getEx1 <em>Ex1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ex1</em>'.
   * @see org.xtext.example.mydsl.myDsl.Foreach#getEx1()
   * @see #getForeach()
   * @generated
   */
  EReference getForeach_Ex1();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.Foreach#getEx2 <em>Ex2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ex2</em>'.
   * @see org.xtext.example.mydsl.myDsl.Foreach#getEx2()
   * @see #getForeach()
   * @generated
   */
  EReference getForeach_Ex2();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.Foreach#getC <em>C</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>C</em>'.
   * @see org.xtext.example.mydsl.myDsl.Foreach#getC()
   * @see #getForeach()
   * @generated
   */
  EReference getForeach_C();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.Ifconfort <em>Ifconfort</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ifconfort</em>'.
   * @see org.xtext.example.mydsl.myDsl.Ifconfort
   * @generated
   */
  EClass getIfconfort();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.Ifconfort#getEx <em>Ex</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ex</em>'.
   * @see org.xtext.example.mydsl.myDsl.Ifconfort#getEx()
   * @see #getIfconfort()
   * @generated
   */
  EReference getIfconfort_Ex();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.Ifconfort#getC <em>C</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>C</em>'.
   * @see org.xtext.example.mydsl.myDsl.Ifconfort#getC()
   * @see #getIfconfort()
   * @generated
   */
  EReference getIfconfort_C();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.Vars <em>Vars</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Vars</em>'.
   * @see org.xtext.example.mydsl.myDsl.Vars
   * @generated
   */
  EClass getVars();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.myDsl.Vars#getVs <em>Vs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Vs</em>'.
   * @see org.xtext.example.mydsl.myDsl.Vars#getVs()
   * @see #getVars()
   * @generated
   */
  EReference getVars_Vs();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.Exprs <em>Exprs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Exprs</em>'.
   * @see org.xtext.example.mydsl.myDsl.Exprs
   * @generated
   */
  EClass getExprs();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.myDsl.Exprs#getEx <em>Ex</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Ex</em>'.
   * @see org.xtext.example.mydsl.myDsl.Exprs#getEx()
   * @see #getExprs()
   * @generated
   */
  EReference getExprs_Ex();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.Expr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expr</em>'.
   * @see org.xtext.example.mydsl.myDsl.Expr
   * @generated
   */
  EClass getExpr();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.Expr#getExs <em>Exs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exs</em>'.
   * @see org.xtext.example.mydsl.myDsl.Expr#getExs()
   * @see #getExpr()
   * @generated
   */
  EReference getExpr_Exs();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.Expr#getExa <em>Exa</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exa</em>'.
   * @see org.xtext.example.mydsl.myDsl.Expr#getExa()
   * @see #getExpr()
   * @generated
   */
  EReference getExpr_Exa();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.ExprSimple <em>Expr Simple</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expr Simple</em>'.
   * @see org.xtext.example.mydsl.myDsl.ExprSimple
   * @generated
   */
  EClass getExprSimple();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDsl.ExprSimple#getNil <em>Nil</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Nil</em>'.
   * @see org.xtext.example.mydsl.myDsl.ExprSimple#getNil()
   * @see #getExprSimple()
   * @generated
   */
  EAttribute getExprSimple_Nil();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.ExprSimple#getV <em>V</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>V</em>'.
   * @see org.xtext.example.mydsl.myDsl.ExprSimple#getV()
   * @see #getExprSimple()
   * @generated
   */
  EReference getExprSimple_V();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.ExprSimple#getSym <em>Sym</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Sym</em>'.
   * @see org.xtext.example.mydsl.myDsl.ExprSimple#getSym()
   * @see #getExprSimple()
   * @generated
   */
  EReference getExprSimple_Sym();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDsl.ExprSimple#getMot <em>Mot</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Mot</em>'.
   * @see org.xtext.example.mydsl.myDsl.ExprSimple#getMot()
   * @see #getExprSimple()
   * @generated
   */
  EAttribute getExprSimple_Mot();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.ExprSimple#getLex <em>Lex</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Lex</em>'.
   * @see org.xtext.example.mydsl.myDsl.ExprSimple#getLex()
   * @see #getExprSimple()
   * @generated
   */
  EReference getExprSimple_Lex();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.ExprSimple#getEx <em>Ex</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ex</em>'.
   * @see org.xtext.example.mydsl.myDsl.ExprSimple#getEx()
   * @see #getExprSimple()
   * @generated
   */
  EReference getExprSimple_Ex();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.LExpr <em>LExpr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>LExpr</em>'.
   * @see org.xtext.example.mydsl.myDsl.LExpr
   * @generated
   */
  EClass getLExpr();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.myDsl.LExpr#getE <em>E</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>E</em>'.
   * @see org.xtext.example.mydsl.myDsl.LExpr#getE()
   * @see #getLExpr()
   * @generated
   */
  EReference getLExpr_E();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.ExprAnd <em>Expr And</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expr And</em>'.
   * @see org.xtext.example.mydsl.myDsl.ExprAnd
   * @generated
   */
  EClass getExprAnd();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.ExprAnd#getExo1 <em>Exo1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exo1</em>'.
   * @see org.xtext.example.mydsl.myDsl.ExprAnd#getExo1()
   * @see #getExprAnd()
   * @generated
   */
  EReference getExprAnd_Exo1();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.myDsl.ExprAnd#getExo2 <em>Exo2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Exo2</em>'.
   * @see org.xtext.example.mydsl.myDsl.ExprAnd#getExo2()
   * @see #getExprAnd()
   * @generated
   */
  EReference getExprAnd_Exo2();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.ExprOr <em>Expr Or</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expr Or</em>'.
   * @see org.xtext.example.mydsl.myDsl.ExprOr
   * @generated
   */
  EClass getExprOr();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.ExprOr#getExn1 <em>Exn1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exn1</em>'.
   * @see org.xtext.example.mydsl.myDsl.ExprOr#getExn1()
   * @see #getExprOr()
   * @generated
   */
  EReference getExprOr_Exn1();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.myDsl.ExprOr#getExn2 <em>Exn2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Exn2</em>'.
   * @see org.xtext.example.mydsl.myDsl.ExprOr#getExn2()
   * @see #getExprOr()
   * @generated
   */
  EReference getExprOr_Exn2();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.ExprNot <em>Expr Not</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expr Not</em>'.
   * @see org.xtext.example.mydsl.myDsl.ExprNot
   * @generated
   */
  EClass getExprNot();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.ExprNot#getExQ1 <em>Ex Q1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ex Q1</em>'.
   * @see org.xtext.example.mydsl.myDsl.ExprNot#getExQ1()
   * @see #getExprNot()
   * @generated
   */
  EReference getExprNot_ExQ1();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.ExprNot#getExQ2 <em>Ex Q2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ex Q2</em>'.
   * @see org.xtext.example.mydsl.myDsl.ExprNot#getExQ2()
   * @see #getExprNot()
   * @generated
   */
  EReference getExprNot_ExQ2();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.ExprEq <em>Expr Eq</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expr Eq</em>'.
   * @see org.xtext.example.mydsl.myDsl.ExprEq
   * @generated
   */
  EClass getExprEq();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.ExprEq#getExS1 <em>Ex S1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ex S1</em>'.
   * @see org.xtext.example.mydsl.myDsl.ExprEq#getExS1()
   * @see #getExprEq()
   * @generated
   */
  EReference getExprEq_ExS1();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.ExprEq#getExS2 <em>Ex S2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ex S2</em>'.
   * @see org.xtext.example.mydsl.myDsl.ExprEq#getExS2()
   * @see #getExprEq()
   * @generated
   */
  EReference getExprEq_ExS2();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDsl.ExprEq#getEx <em>Ex</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Ex</em>'.
   * @see org.xtext.example.mydsl.myDsl.ExprEq#getEx()
   * @see #getExprEq()
   * @generated
   */
  EReference getExprEq_Ex();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.VAR <em>VAR</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>VAR</em>'.
   * @see org.xtext.example.mydsl.myDsl.VAR
   * @generated
   */
  EClass getVAR();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDsl.VAR#getBv <em>Bv</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Bv</em>'.
   * @see org.xtext.example.mydsl.myDsl.VAR#getBv()
   * @see #getVAR()
   * @generated
   */
  EAttribute getVAR_Bv();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDsl.VAR#getCf <em>Cf</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Cf</em>'.
   * @see org.xtext.example.mydsl.myDsl.VAR#getCf()
   * @see #getVAR()
   * @generated
   */
  EAttribute getVAR_Cf();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.SYMB <em>SYMB</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>SYMB</em>'.
   * @see org.xtext.example.mydsl.myDsl.SYMB
   * @generated
   */
  EClass getSYMB();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDsl.SYMB#getBs <em>Bs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Bs</em>'.
   * @see org.xtext.example.mydsl.myDsl.SYMB#getBs()
   * @see #getSYMB()
   * @generated
   */
  EAttribute getSYMB_Bs();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDsl.SYMB#getCf <em>Cf</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Cf</em>'.
   * @see org.xtext.example.mydsl.myDsl.SYMB#getCf()
   * @see #getSYMB()
   * @generated
   */
  EAttribute getSYMB_Cf();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  MyDslFactory getMyDslFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.ModelImpl <em>Model</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.ModelImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getModel()
     * @generated
     */
    EClass MODEL = eINSTANCE.getModel();

    /**
     * The meta object literal for the '<em><b>Prog</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MODEL__PROG = eINSTANCE.getModel_Prog();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.FunctionImpl <em>Function</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.FunctionImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getFunction()
     * @generated
     */
    EClass FUNCTION = eINSTANCE.getFunction();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FUNCTION__NAME = eINSTANCE.getFunction_Name();

    /**
     * The meta object literal for the '<em><b>Def</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FUNCTION__DEF = eINSTANCE.getFunction_Def();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.DefinitionImpl <em>Definition</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.DefinitionImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getDefinition()
     * @generated
     */
    EClass DEFINITION = eINSTANCE.getDefinition();

    /**
     * The meta object literal for the '<em><b>In</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DEFINITION__IN = eINSTANCE.getDefinition_In();

    /**
     * The meta object literal for the '<em><b>Com</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DEFINITION__COM = eINSTANCE.getDefinition_Com();

    /**
     * The meta object literal for the '<em><b>Out</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DEFINITION__OUT = eINSTANCE.getDefinition_Out();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.InputImpl <em>Input</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.InputImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getInput()
     * @generated
     */
    EClass INPUT = eINSTANCE.getInput();

    /**
     * The meta object literal for the '<em><b>V</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INPUT__V = eINSTANCE.getInput_V();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.OutputImpl <em>Output</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.OutputImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getOutput()
     * @generated
     */
    EClass OUTPUT = eINSTANCE.getOutput();

    /**
     * The meta object literal for the '<em><b>V</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OUTPUT__V = eINSTANCE.getOutput_V();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.CommandsImpl <em>Commands</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.CommandsImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getCommands()
     * @generated
     */
    EClass COMMANDS = eINSTANCE.getCommands();

    /**
     * The meta object literal for the '<em><b>C</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COMMANDS__C = eINSTANCE.getCommands_C();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.CommandImpl <em>Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.CommandImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getCommand()
     * @generated
     */
    EClass COMMAND = eINSTANCE.getCommand();

    /**
     * The meta object literal for the '<em><b>Nop</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COMMAND__NOP = eINSTANCE.getCommand_Nop();

    /**
     * The meta object literal for the '<em><b>Assign</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COMMAND__ASSIGN = eINSTANCE.getCommand_Assign();

    /**
     * The meta object literal for the '<em><b>Wh</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COMMAND__WH = eINSTANCE.getCommand_Wh();

    /**
     * The meta object literal for the '<em><b>For</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COMMAND__FOR = eINSTANCE.getCommand_For();

    /**
     * The meta object literal for the '<em><b>If</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COMMAND__IF = eINSTANCE.getCommand_If();

    /**
     * The meta object literal for the '<em><b>Fore</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COMMAND__FORE = eINSTANCE.getCommand_Fore();

    /**
     * The meta object literal for the '<em><b>Ifc</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COMMAND__IFC = eINSTANCE.getCommand_Ifc();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.AssignImpl <em>Assign</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.AssignImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getAssign()
     * @generated
     */
    EClass ASSIGN = eINSTANCE.getAssign();

    /**
     * The meta object literal for the '<em><b>Vs</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ASSIGN__VS = eINSTANCE.getAssign_Vs();

    /**
     * The meta object literal for the '<em><b>Ex</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ASSIGN__EX = eINSTANCE.getAssign_Ex();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.WhileImpl <em>While</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.WhileImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getWhile()
     * @generated
     */
    EClass WHILE = eINSTANCE.getWhile();

    /**
     * The meta object literal for the '<em><b>Ex</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference WHILE__EX = eINSTANCE.getWhile_Ex();

    /**
     * The meta object literal for the '<em><b>C</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference WHILE__C = eINSTANCE.getWhile_C();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.ForImpl <em>For</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.ForImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getFor()
     * @generated
     */
    EClass FOR = eINSTANCE.getFor();

    /**
     * The meta object literal for the '<em><b>Ex</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FOR__EX = eINSTANCE.getFor_Ex();

    /**
     * The meta object literal for the '<em><b>C</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FOR__C = eINSTANCE.getFor_C();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.IfImpl <em>If</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.IfImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getIf()
     * @generated
     */
    EClass IF = eINSTANCE.getIf();

    /**
     * The meta object literal for the '<em><b>Ex</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IF__EX = eINSTANCE.getIf_Ex();

    /**
     * The meta object literal for the '<em><b>Ct</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IF__CT = eINSTANCE.getIf_Ct();

    /**
     * The meta object literal for the '<em><b>Ce</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IF__CE = eINSTANCE.getIf_Ce();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.ForeachImpl <em>Foreach</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.ForeachImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getForeach()
     * @generated
     */
    EClass FOREACH = eINSTANCE.getForeach();

    /**
     * The meta object literal for the '<em><b>Ex1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FOREACH__EX1 = eINSTANCE.getForeach_Ex1();

    /**
     * The meta object literal for the '<em><b>Ex2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FOREACH__EX2 = eINSTANCE.getForeach_Ex2();

    /**
     * The meta object literal for the '<em><b>C</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FOREACH__C = eINSTANCE.getForeach_C();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.IfconfortImpl <em>Ifconfort</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.IfconfortImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getIfconfort()
     * @generated
     */
    EClass IFCONFORT = eINSTANCE.getIfconfort();

    /**
     * The meta object literal for the '<em><b>Ex</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IFCONFORT__EX = eINSTANCE.getIfconfort_Ex();

    /**
     * The meta object literal for the '<em><b>C</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IFCONFORT__C = eINSTANCE.getIfconfort_C();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.VarsImpl <em>Vars</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.VarsImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getVars()
     * @generated
     */
    EClass VARS = eINSTANCE.getVars();

    /**
     * The meta object literal for the '<em><b>Vs</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VARS__VS = eINSTANCE.getVars_Vs();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.ExprsImpl <em>Exprs</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.ExprsImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getExprs()
     * @generated
     */
    EClass EXPRS = eINSTANCE.getExprs();

    /**
     * The meta object literal for the '<em><b>Ex</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPRS__EX = eINSTANCE.getExprs_Ex();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.ExprImpl <em>Expr</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.ExprImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getExpr()
     * @generated
     */
    EClass EXPR = eINSTANCE.getExpr();

    /**
     * The meta object literal for the '<em><b>Exs</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPR__EXS = eINSTANCE.getExpr_Exs();

    /**
     * The meta object literal for the '<em><b>Exa</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPR__EXA = eINSTANCE.getExpr_Exa();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.ExprSimpleImpl <em>Expr Simple</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.ExprSimpleImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getExprSimple()
     * @generated
     */
    EClass EXPR_SIMPLE = eINSTANCE.getExprSimple();

    /**
     * The meta object literal for the '<em><b>Nil</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute EXPR_SIMPLE__NIL = eINSTANCE.getExprSimple_Nil();

    /**
     * The meta object literal for the '<em><b>V</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPR_SIMPLE__V = eINSTANCE.getExprSimple_V();

    /**
     * The meta object literal for the '<em><b>Sym</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPR_SIMPLE__SYM = eINSTANCE.getExprSimple_Sym();

    /**
     * The meta object literal for the '<em><b>Mot</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute EXPR_SIMPLE__MOT = eINSTANCE.getExprSimple_Mot();

    /**
     * The meta object literal for the '<em><b>Lex</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPR_SIMPLE__LEX = eINSTANCE.getExprSimple_Lex();

    /**
     * The meta object literal for the '<em><b>Ex</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPR_SIMPLE__EX = eINSTANCE.getExprSimple_Ex();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.LExprImpl <em>LExpr</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.LExprImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getLExpr()
     * @generated
     */
    EClass LEXPR = eINSTANCE.getLExpr();

    /**
     * The meta object literal for the '<em><b>E</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LEXPR__E = eINSTANCE.getLExpr_E();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.ExprAndImpl <em>Expr And</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.ExprAndImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getExprAnd()
     * @generated
     */
    EClass EXPR_AND = eINSTANCE.getExprAnd();

    /**
     * The meta object literal for the '<em><b>Exo1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPR_AND__EXO1 = eINSTANCE.getExprAnd_Exo1();

    /**
     * The meta object literal for the '<em><b>Exo2</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPR_AND__EXO2 = eINSTANCE.getExprAnd_Exo2();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.ExprOrImpl <em>Expr Or</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.ExprOrImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getExprOr()
     * @generated
     */
    EClass EXPR_OR = eINSTANCE.getExprOr();

    /**
     * The meta object literal for the '<em><b>Exn1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPR_OR__EXN1 = eINSTANCE.getExprOr_Exn1();

    /**
     * The meta object literal for the '<em><b>Exn2</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPR_OR__EXN2 = eINSTANCE.getExprOr_Exn2();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.ExprNotImpl <em>Expr Not</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.ExprNotImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getExprNot()
     * @generated
     */
    EClass EXPR_NOT = eINSTANCE.getExprNot();

    /**
     * The meta object literal for the '<em><b>Ex Q1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPR_NOT__EX_Q1 = eINSTANCE.getExprNot_ExQ1();

    /**
     * The meta object literal for the '<em><b>Ex Q2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPR_NOT__EX_Q2 = eINSTANCE.getExprNot_ExQ2();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.ExprEqImpl <em>Expr Eq</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.ExprEqImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getExprEq()
     * @generated
     */
    EClass EXPR_EQ = eINSTANCE.getExprEq();

    /**
     * The meta object literal for the '<em><b>Ex S1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPR_EQ__EX_S1 = eINSTANCE.getExprEq_ExS1();

    /**
     * The meta object literal for the '<em><b>Ex S2</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPR_EQ__EX_S2 = eINSTANCE.getExprEq_ExS2();

    /**
     * The meta object literal for the '<em><b>Ex</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPR_EQ__EX = eINSTANCE.getExprEq_Ex();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.VARImpl <em>VAR</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.VARImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getVAR()
     * @generated
     */
    EClass VAR = eINSTANCE.getVAR();

    /**
     * The meta object literal for the '<em><b>Bv</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VAR__BV = eINSTANCE.getVAR_Bv();

    /**
     * The meta object literal for the '<em><b>Cf</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VAR__CF = eINSTANCE.getVAR_Cf();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.SYMBImpl <em>SYMB</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.SYMBImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getSYMB()
     * @generated
     */
    EClass SYMB = eINSTANCE.getSYMB();

    /**
     * The meta object literal for the '<em><b>Bs</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SYMB__BS = eINSTANCE.getSYMB_Bs();

    /**
     * The meta object literal for the '<em><b>Cf</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SYMB__CF = eINSTANCE.getSYMB_Cf();

  }

} //MyDslPackage
