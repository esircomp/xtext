package tests;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.xtext.example.mydsl.GeneratorPP;
import org.xtext.example.mydsl.PrettyPrinter;
import org.xtext.example.mydsl.beans.ArgumentsPP;
import org.xtext.example.mydsl.generator.MyDslGenerator;

public abstract class AbstractTest {

	protected static final String OK   = "[OK]    " + " ";
	protected static final String FAIL = "[FAILED]" + " ";
	protected static final String ND   = "[ND]    " + " ";
	protected String rapport;
	protected String entry;
	protected boolean estValide;
	private MyDslGenerator	generator;

	public AbstractTest(){
		generator = new MyDslGenerator();
	}
	/**
	 * @return Le resultat du test
	 */
	public boolean estValide(){
		return estValide;
	}
	/**
	 * Execute le test et met a true ou false le resultat qui en decoule.
	 */
	public abstract void tester();

	/**
	 *  Genere un fichier du meme nom de classe, pour une entree donnee.
	 */
	public String genererRapport(){
		FileWriter fw;
		try {
			fw = new FileWriter("src/results/" + entry + ".result", true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write("\n" + rapport);
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return rapport;
	}

	public void setEntry(String entry){
		this.entry = entry;
		rapport = ND + getClass().getSimpleName() + " : Cette classe de test n'a pas encore été implémenté.";
		estValide = false;
	}

	public File prettyPrinting(String path, String file){
		GeneratorPP generator = new GeneratorPP();
		generator.generate((new File(path+file)).getAbsolutePath(), file+"pp", PrettyPrinter.generateMap(new ArgumentsPP()), false);
		return new File(path+"/outputs/"+file+"pp");
	}
}
