package org.xtext.example.mydsl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Scanner;

import org.xtext.example.mydsl.beans.ArgumentsPP;

import com.beust.jcommander.JCommander;

public class PrettyPrinter {
	private static final String	absolutePathHelp	= "/org/xtext/example/mydsl/Help.txt";
	public static void main(String[] args) {

		if (args.length == 0) {
			System.out.println("Il faut préciser le fichier d'entré");
			System.out.println("-help pour plus d'informations");
			return;
		}
		ArgumentsPP arguments = new ArgumentsPP();
		try {
			new JCommander(arguments, args);
		}
		catch (Exception e) {
			System.out.println("Les paramètres doivent être valides");
			return;
		}

		if (arguments.help) {
			URL inputUrl = (new Compilateur()).getClass().getResource(absolutePathHelp);
			InputStream in = (new Compilateur()).getClass().getResourceAsStream(absolutePathHelp); 
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			String s="";
			try {
				while ((s=reader.readLine())!=null)
				{

				       System.out.println(s);
				}
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			return;
		}

		String outputFile = "";
		String inputFile = args[0];

		File file = new File(inputFile);
		if (!file.exists()) {
			System.out.println("Ce fichier n'existe pas");
			return;
		}

		HashMap<String, String> map = generateMap(arguments);

		if (arguments.output != null && arguments.output.length() > 0) {
			outputFile = arguments.output;
		}
		else {
			outputFile = file.getName().substring(0, file.getName().lastIndexOf('.') + 1) + "whpp";
		}
		System.out.println("Fichier de sortie : " + outputFile);

		GeneratorPP generator = new GeneratorPP();
		generator.generate(inputFile, outputFile, map, false);


	}

	public static HashMap<String, String> generateMap(ArgumentsPP arguments) {
		HashMap<String, String> map = new HashMap<String, String>();
		if (arguments.pWhile == null)
			map.put("while", getSpaces(arguments.all));
		else
			map.put("while", getSpaces(arguments.pWhile));
		if (arguments.pFor == null)
			map.put("for", getSpaces(arguments.all));
		else
			map.put("for", getSpaces(arguments.pFor));
		if (arguments.pForeach == null)
			map.put("foreach", getSpaces(arguments.all));
		else
			map.put("foreach", getSpaces(arguments.pForeach));
		if (arguments.pIf == null)
			map.put("if", getSpaces(arguments.all));
		else
			map.put("if", getSpaces(arguments.pIf));
		map.put("all", getSpaces(arguments.all));
		return map;
	}

	public static String getSpaces(Integer nb) {
		String str = "";
		for (int i = 0; i < nb; i++)
			str += " ";
		return str;
	}

}
