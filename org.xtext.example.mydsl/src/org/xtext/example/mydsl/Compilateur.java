package org.xtext.example.mydsl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import javax.lang.model.util.Types;
import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import org.apache.commons.io.FileUtils;
import org.xtext.example.mydsl.beans.ArgumentsPP;
import org.xtext.example.mydsl.beans.BinTree;
import org.xtext.example.mydsl.beans.SymbFunction;
import org.xtext.example.mydsl.beans.SymbTable;

import com.beust.jcommander.JCommander;

public class Compilateur {
	private static final Path	currentRelativePath	= Paths.get("");
	private static final String	s					= currentRelativePath.toAbsolutePath().toString();
	private static final String	filePath			= s + "/tmpJava/";
	private static final String	filePathBin			= s + "/compileBin/";
	private static final String	filePathBinBinTree	= s + "/compileBin/org/xtext/example/mydsl/beans/BinTree.class";
	private static final String	filePathBinTree		= s + "/src/org/xtext/example/mydsl/beans/BinTree.java";
	private static final String	absolutePathBinTree	= "/org/xtext/example/mydsl/beans/BinTree.class";

	public static void main(String[] args) {

		// args = new String[]{"C:/Users/thomas/git/xtext/Tests/test2.wh",
		// "-symbole"};
		// args = new
		// String[]{"/Users/DUARTE/Documents/workspaceXTEXT/xTextProjet/Tests/test3.wh",
		// "-j"};
		if (args.length == 0) {

			System.out.println("Il faut préciser le fichier d'entré");
			System.out.println("-help pour plus d'informations");
			return;
		}
		ArgumentsPP arguments = new ArgumentsPP();
		try {
			new JCommander(arguments, args);
		}
		catch (Exception e) {
			System.out.println("Les paramètres doivent être valides");
			return;
		}

		if (arguments.help) {

			File fileHelp = null;
			try {
				fileHelp = new File("help.txt");

				Scanner scanner = new Scanner(fileHelp);
				scanner.useDelimiter("\r\n");
				while (scanner.hasNext()) {
					String line = scanner.next();
					String cells[] = line.split("\t");
					System.out.println(line);
				}
				scanner.close();
			}
			catch (Exception e) {
				
				System.out.println(new File("Help.txt").getAbsolutePath());
				System.out.println(e);
			}
			return;
		}

		String outputFile = "";
		String inputFile = args[0];

		File file = new File(inputFile);
		if (!file.exists()) {
			System.out.println("Ce fichier n'existe pas");
			return;
		}

		if (arguments.output != null && arguments.output.length() > 0) {
			outputFile = arguments.output;
		}
		else {
			outputFile = file.getName().substring(0, file.getName().lastIndexOf('.') + 1) + "whpp";
		}
		System.out.println("Fichier de sortie : " + outputFile);
		
		
		if(args.length>1){
			System.out.println("parametres reçus OK");
			String[] para = new String[args.length-1];
			int countPara=0;
			for (int i = 1; i < args.length; i++) {
				// parameters[i] = String.valueOf(((int) (Math.random() * 10)));
				para[countPara] = args[i];
				System.out.println("parametres reçus "+countPara+" :" + para[countPara]);
				countPara++;
			}
			generateJava(inputFile,para);
		}else{
			generateJava(inputFile);
		}
		

	}

	public static boolean generateJava(Object type, String inputFile){
		return generateJava(type, inputFile, null);
	}
	public static boolean generateJava(String inputFile) {

		return generateJava(new Integer(1), inputFile, new String[0]);
	}
	public static boolean generateJava(String inputFile, String[] parametres) {

		return generateJava(new Integer(1), inputFile, parametres);
	}

	public static boolean generateJava(Object type, String inputFile, String[] parametres) {
		// base folders
		File theDir = new File(filePath);
		// if the directory does not exist, create it
		if (!theDir.exists()) {
			theDir.mkdir();
		}
		theDir = new File(filePathBin);
		// if the directory does not exist, create it
		if (!theDir.exists()) {
			theDir.mkdir();
		}

		GeneratorSym generator = null;
		try {

			generator = new GeneratorSym();
			generator.generate(inputFile);
		}
		catch (Exception e) {
			System.out.println(
					"Erreur de syntaxe dans votre fichier while: " + inputFile + " Erreurs :" );
			e.printStackTrace();
			return false;
		}

		String fileName = new File(inputFile).getName();
		fileName = fileName.replace(".wh", ".java");

		System.out.println("lastFunctionName : " + SymbTable.getLastFunction().value);
		String st = addJavaAditional(type, fileName, generator.getSymTable().getJAVA(), SymbTable.getLastFunction());

		FileWriter fw;
		try {
			fw = new FileWriter(filePath + fileName);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(st.toString());
			bw.close();

		}
		catch (IOException e) {
			e.printStackTrace();
		}

		// Copy additionnal Imports

		
		URL inputUrl = (new Compilateur()).getClass().getResource(absolutePathBinTree);
		File dest = new File(filePathBinBinTree);
		try {
			FileUtils.copyURLToFile(inputUrl, dest);
		}
		catch (IOException e1) {

			e1.printStackTrace();
		}

		// gestion des parametres
		int sizeParam = generator.getSymTable().getLastFunction().inputs.size();
		Object[] paraObj=null;
		

		return compileJava(fileName, filePath + fileName, SymbTable.getLastFunction())
				& executeJava(fileName, parametres);

	}

	public static boolean compileJava(String javaFileName, String javaFilePath, SymbFunction mainLastFonction) {
		// Compile source file.
		System.setProperty("java.home", "C:\\Program Files\\Java\\jdk1.8.0_60");
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		if (compiler == null) {
			System.setProperty("java.home", "C:\\Program Files\\Java\\jdk1.8.0_66");
			compiler = ToolProvider.getSystemJavaCompiler();
		}
		if (compiler == null) {
			System.setProperty("java.home", "/Library/Java/JavaVirtualMachines/jdk1.8.0_25.jdk/Contents/Home");
			compiler = ToolProvider.getSystemJavaCompiler();
		}
		while (compiler == null) {
			Scanner sc = new Scanner(System.in);
			System.out.println("chemin du jdk incorrect : Veuillez saisir le chemin du jdk :");
			String str = sc.nextLine();
			System.out.println("Vous avez saisi : " + str);
			System.setProperty("java.home", str);
			compiler = ToolProvider.getSystemJavaCompiler();
		}

		StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);

		// prepare the source file(s) to compile
		List<File> sourceFileList = new ArrayList<File>();
		// sourceFileList.add (new File (filePathBinTree));
		sourceFileList.add(new File(javaFilePath));
		Iterable<? extends JavaFileObject> compilationUnits = fileManager.getJavaFileObjectsFromFiles(sourceFileList);

		final Iterable<String> options = Arrays.asList(new String[] { "-d", filePathBin });
		CompilationTask task = compiler.getTask(null, fileManager, null, options, null, compilationUnits);

		boolean result = task.call();
		if (result) {
			System.out.println("Compilation was successful");
		}
		else {
			System.out.println("Compilation failed");
			return false;
		}

		return true;

	}

	public static boolean executeJava(String javaFileName, Object[] parametres) {
		Runtime rt = Runtime.getRuntime();
		try {

			URLClassLoader classLoader = URLClassLoader
					.newInstance(new URL[] { (new File(filePathBin)).toURI().toURL() });

			Class<?> loadedClass = classLoader.loadClass(javaFileName.replace(".java", ""));

			Object instance = loadedClass.newInstance();
			// call the printIt method

			Method method = loadedClass.getDeclaredMethod("main", parametres.getClass());
			Object[] param = { parametres };
			for (int i = 0; i < param.length; i++) {
				System.out.println("Parametres d'execution : "+i+" "+param[i]);
			}
			method.invoke(instance, param);
		}
		catch (Exception e) {
			System.out.println("Erreur lors de l'execution");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static String executeJavaAndGetRst(String javaFileName, Object[] parametres) {
		Runtime rt = Runtime.getRuntime();
		try {

			URLClassLoader classLoader = URLClassLoader
					.newInstance(new URL[] { (new File(filePathBin)).toURI().toURL() });

			Class<?> loadedClass = classLoader.loadClass(javaFileName.replace(".java", ""));

			Object instance = loadedClass.newInstance();
			// call the printIt method

			Method method = loadedClass.getDeclaredMethod("main", parametres.getClass());
			Object[] param = { parametres };
			return (String) method.invoke(instance, param);
		}
		catch (Exception e) {
			System.out.println("Erreur lors de l'execution");
			return e.getMessage();

		}
	}

	public static String addJavaAditional(Object type, String fileName, String java, SymbFunction nomMainFct) {
		fileName = fileName.replace(".java", "");
		String rst = "import java.util.*;";
		rst += System.lineSeparator();
		rst += "import org.xtext.example.mydsl.beans.BinTree;";
		rst += System.lineSeparator();
		rst += "public class " + fileName + " {";
		rst += System.lineSeparator();
		rst += "public static void main(String[] args) {";
		rst += System.lineSeparator();
		if (type instanceof Integer) {
			rst += "System.out.println(BinTree.toStringListBinTree(" + nomMainFct.value + "(";
			for (int i = 0; i < nomMainFct.inputs.size(); i++) {
				if (i == nomMainFct.inputs.size() - 1) {
					
					rst +="(args.length-1>="+String.valueOf(i)+") ?  new BinTree(Integer.valueOf(args[" + String.valueOf(i) + "])) : BinTree.NIL";
				}
				else {
					rst += "(args.length-1>="+String.valueOf(i)+") ?  new BinTree(Integer.valueOf(args[" + String.valueOf(i) + "])) : BinTree.NIL,";
				}
			}
		}
		else if (type instanceof String) {
			rst += "System.out.println(BinTree.toStringListBinTree(" + nomMainFct.value + "(";
			for (int i = 0; i < nomMainFct.inputs.size(); i++) {

				if (i == nomMainFct.inputs.size() - 1) {
					rst +="(args.length-1>="+String.valueOf(i)+") ?  new BinTree(String.valueOf(args[" + String.valueOf(i) + "])) : BinTree.NIL";
				}
				else {
					rst +="(args.length-1>="+String.valueOf(i)+") ?  new BinTree(String.valueOf(args[" + String.valueOf(i) + "])) : BinTree.NIL,";
				}
			}
		}
		else if (type instanceof Boolean) {
			rst += "System.out.println(BinTree.toStringListBinTree(" + nomMainFct.value + "(";
			for (int i = 0; i < nomMainFct.inputs.size(); i++) {
				if (i == nomMainFct.inputs.size() - 1) {
					rst += "(args.length-1>="+String.valueOf(i)+") ?  new BinTree(Boolean.valueOf(args[" + String.valueOf(i) + "])) : BinTree.NIL";
				}
				else {
					rst +="(args.length-1>="+String.valueOf(i)+") ?  new BinTree(Boolean.valueOf(args[" + String.valueOf(i) + "])) : BinTree.NIL,";
				}
			}
		}
		else {
			rst += "System.out.println(BinTree.toStringListBinTree(" + nomMainFct.value + "(";
			for (int i = 0; i < nomMainFct.inputs.size(); i++) {
				if (i == nomMainFct.inputs.size() - 1) {
					rst +="(args.length-1>="+String.valueOf(i)+") ? new BinTree(Integer.valueOf(args[" + String.valueOf(i) + "])) : BinTree.NIL";
				}
				else {
					rst +="(args.length-1>="+String.valueOf(i)+") ? new BinTree(Integer.valueOf(args[" + String.valueOf(i) + "])) : BinTree.NIL,";
				}
			}
		}

		rst += ")));";
		rst += System.lineSeparator();
		rst += "} ";
		rst += System.lineSeparator();
		rst += java;
		rst += "}";
		rst += System.lineSeparator();
		return rst;
	}

	static public String ExportResource(String resourceName) {
		InputStream stream = null;
		OutputStream resStreamOut = null;
		String jarFolder = null;
		try {
			stream = Compilateur.class.getResourceAsStream(resourceName);// note
																			// that
																			// each
																			// /
																			// is
																			// a
																			// directory
																			// down
																			// in
																			// the
																			// "jar
																			// tree"
																			// been
																			// the
																			// jar
																			// the
																			// root
																			// of
																			// the
																			// tree
			if (stream == null) {
				throw new Exception("Cannot get resource \"" + resourceName + "\" from Jar file.");
			}

			int readBytes;
			byte[] buffer = new byte[4096];
			jarFolder = new File(
					Compilateur.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath())
							.getParentFile().getPath().replace('\\', '/');
			resStreamOut = new FileOutputStream(jarFolder + resourceName);
			while ((readBytes = stream.read(buffer)) > 0) {
				resStreamOut.write(buffer, 0, readBytes);
			}

		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		finally {

			try {
				stream.close();
				resStreamOut.close();
			}
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return jarFolder + resourceName;
	}
}
