package org.xtext.example.mydsl;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.xtext.generator.JavaIoFileSystemAccess;
import org.xtext.example.mydsl.beans.SymbTable;
import org.xtext.example.mydsl.generator.MyDslGeneratorSym;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;

public class GeneratorSym {
	@Inject
	private Provider<ResourceSet>	resourceSetProvider;

	@Inject
	private MyDslGeneratorSym			myDslGenerator;

	@Inject
	private JavaIoFileSystemAccess	fileAccess;

	public SymbTable				symbTable;
	private GeneratorSym				generator;

	public void runGenerator(String inputFile) {
		// load the resource
		ResourceSet set = resourceSetProvider.get();
		Resource resource = set.getResource(URI.createFileURI(inputFile), true);
		SymbTable.reset();
		symbTable = new SymbTable();
		myDslGenerator.generate(resource, fileAccess, symbTable);
	}

	public void generate(String inputFile) {
		Injector injector = new MyDslStandaloneSetup().createInjectorAndDoEMFRegistration();
		generator = injector.getInstance(GeneratorSym.class);
		generator.runGenerator(inputFile);
	}

	public SymbTable getSymTable() {
		return generator.symbTable;
	}

}
