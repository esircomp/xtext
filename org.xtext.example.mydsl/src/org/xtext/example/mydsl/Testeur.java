package org.xtext.example.mydsl;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import tests.AbstractTest;
import tests.StressTest;

public class Testeur {


	/**
	 * Testeur, sans option.
	 */
	public Testeur(){
	}

	// Application des tests
	public void testerPrettyPrinter(){
		System.out.println("========== StressTest ==========");
		AbstractTest stressTest = new StressTest();
		stressTest.tester();
		((StressTest) stressTest).genererGraphique(5);
		System.out.println(stressTest.genererRapport());
		
	}

	public void reinitialiserRapport(String entry, String label){
		FileWriter fw;
		try {
			fw = new FileWriter("src/results/" + entry + ".result");
			BufferedWriter bw = new BufferedWriter(fw);
			SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
			Date today = new Date(System.currentTimeMillis());
			bw.write(formatter.format(today) + " - " + label);
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}



	public static void main(String[] args) {
		Testeur testeur = new Testeur();
		testeur.testerPrettyPrinter();

	}
}