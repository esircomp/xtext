package org.xtext.example.mydsl.beans;

public class CodeOP {

	public static final int	EGAL	= 1, AFF = 2, HD = 3, TL = 4, CONS = 5, AND = 6, OR = 7, NOT = 8;
	public int				codeOP;

	public CodeOP(int codeOP) {
		this.codeOP = codeOP;
	}

	public String getCodeOP() {
		switch (codeOP) {
		case EGAL:
			return ").equals(";
		case AFF:
			return " = ";
		case OR:
			return " || ";
		case AND:
			return " && ";
		default:
			return "";
		}
	}
}
