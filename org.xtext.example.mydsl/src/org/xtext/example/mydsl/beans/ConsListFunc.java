package org.xtext.example.mydsl.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class ConsListFunc {

	public static final int			CONS	= 1, LIST = 2, VAR = 3, FUNC = 4, SYMB = 5;
	private static final String		NIL		= "nil";
	private List<ConsListFunc>		vars;
	private String					value;
	private int						type;
	private HashMap<String, String>	map;

	public ConsListFunc(int type) {
		this.type = type;
		vars = new ArrayList<ConsListFunc>();
	}

	public ConsListFunc(int type, String value) {
		this.type = type;
		this.value = value;
		vars = new ArrayList<ConsListFunc>();
	}

	public void addVar(String var) {
		vars.add(new ConsListFunc(VAR, var));
	}

	public void addSymb(String symb) {
		vars.add(new ConsListFunc(SYMB, symb));
	}

	public ConsListFunc addCons() {
		ConsListFunc cons = new ConsListFunc(CONS);
		vars.add(cons);
		return cons;
	}

	public ConsListFunc addList() {
		ConsListFunc list = new ConsListFunc(LIST);
		vars.add(list);
		return list;
	}

	public ConsListFunc addFunc(String func) {
		ConsListFunc function = new ConsListFunc(FUNC, func);
		vars.add(function);
		return function;
	}

	public String toString() {
		String ret = "";
		switch (type) {
		case CONS:
			ret += "BinTree.cons(";
			for (ConsListFunc c : vars) {
				boolean found = false;
				if (map != null) {
					for (Entry<String, String> e : map.entrySet()) {
						if (e.getKey().equals(c.value)) {
							found = true;
							ret += e.getValue() + ", ";
						}
					}
				}
				if (!found)
					ret += c.toString() + ", ";
			}
			if (vars.size() > 0)
				ret = ret.substring(0, ret.length() - 2);
			ret += ")";
			break;
		case FUNC:
			ret += "(" + SymbTable.getFunctionName(value) + "(";
			for (ConsListFunc c : vars) {
				ret += c.toString() + ", ";
			}
			if (vars.size() > 0)
				ret = ret.substring(0, ret.length() - 2);
			ret += "))";
			break;
		case LIST:
			ret += "BinTree.list(";
			boolean func = false;
			for (ConsListFunc c : vars) {
				if (c.type == FUNC) {
					ret = "";
					func = true;
				}
				ret += c.toString() + ", ";
			}
			if (vars.size() > 0)
				ret = ret.substring(0, ret.length() - 2);
			if (!func)
				ret += ")";
			break;
		case VAR:
			if (value.equals(NIL))
				ret += "BinTree.NIL";
			else
				ret += value;
			break;
		case SYMB:
			ret += "new BinTree(\"" + value + "\")";
			break;
		}
		return ret;
	}

	public int getType() {
		return type;
	}

	public void setMap(HashMap<String, String> map) {
		this.map = map;
		for(ConsListFunc c : vars){
			c.setMap(map);
		}
	}
}
