package org.xtext.example.mydsl.beans;

import java.util.List;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.internal.Lists;

public class ArgumentsPP {
	@Parameter
	public List<String> parameters = Lists.newArrayList();

	@Parameter(names = { "-o", "-output" }, description = "Output file")
	public String output;

	@Parameter(names = {"-all", "-a"}, description = "Indentation")
	public Integer all = 2;

	@Parameter(names = {"-while"}, description = "Indentation du while")
	public Integer pWhile;

	@Parameter(names = {"-if"}, description = "Indentation du if")
	public Integer pIf;

	@Parameter(names = {"-for"}, description = "Indentation du for")
	public Integer pFor;

	@Parameter(names = {"-foreach"}, description = "Indentation du foreach")
	public Integer pForeach;
	
	@Parameter(names = {"-help", "-h"}, description = "Help")
	public boolean help = false;
	
}