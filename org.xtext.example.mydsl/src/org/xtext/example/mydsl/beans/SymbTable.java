package org.xtext.example.mydsl.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SymbTable {

	public CharSequence						currentFunction;
	public SymbFunction						currentSymb;
	private List<SymbFunction>				fonctions;
	static Integer							varCount		= 0;
	static Integer							functionCount	= 0;
	static SymbFunction						lastFunction;
	public static HashMap<String, String>	vars;
	public static HashMap<String, String>	functions;

	public static void reset() {
		vars = null;
		functions = null;
		lastFunction = null;
		functionCount = 0;
		varCount = 0;
	}

	public SymbTable() {
		fonctions = new ArrayList<SymbFunction>();
		vars = new HashMap<String, String>();
		functions = new HashMap<String, String>();
	}

	public void addFunction(CharSequence funName) {
		currentSymb = new SymbFunction(String.valueOf(funName));
		fonctions.add(currentSymb);
		lastFunction = currentSymb;
	}

	public SymbFunction get(CharSequence funName) {
		for (SymbFunction s : fonctions) {
			if (s.type == SymbFunction.FUNCTION && s.name.equals(String.valueOf(funName)))
				return s;
		}
		return null;
	}

	public void displayTable() {
		for (SymbFunction s : fonctions) {
			System.out.println(s.toStringSymbole());
		}
	}

	public void display3A() {
		for (SymbFunction s : fonctions) {
			System.out.println(s.toString3A());
		}
	}

	public void displayJAVA() {
		for (SymbFunction s : fonctions) {
			System.out.println(s.toJAVA());
		}
	}

	public String getJAVA() {
		String ret = "";
		for (SymbFunction s : fonctions) {
			ret += s.toJAVA();
		}
		return ret;
	}

	public static SymbFunction getLastFunction() {
		return lastFunction;
	}

	public static void setLastFunction(SymbFunction lastFunction) {
		SymbTable.lastFunction = lastFunction;
	}

	public static String getFunctionName(String funName) {
		return functions.get(funName);
	}

}