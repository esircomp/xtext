package org.xtext.example.mydsl.beans;

import java.util.HashMap;
import java.util.Map.Entry;
import org.xtext.example.mydsl.beans.Expression;

public class Expression {

	public Expression				expr1, expr2;
	public String					var1, var2;
	public CodeOP					codeOP;
	public boolean					not;
	public String					NIL	= "nil";
	public String					HD	= "hd";
	public String					TL	= "tl";
	public static String			CONS	= "cons", LIST = "list", FUNC = "func", SYMB = "symb";
	public ConsListFunc				consList1, consList2, consTmp;
	public boolean					consORlistORsymb	= false, next, symb1, symb2, hd, tl;
	private HashMap<String, String>	map;

	public Expression(Expression expr1, Expression expr2) {
		this.expr1 = expr1;
		this.expr2 = expr2;
	}

	public void addConsList(String conslist) {
		consORlistORsymb = true;
		if (conslist.equals(CONS))
			addCons();
		else if (conslist.equals(LIST))
			addList();
	}

	public void addConsList(String conslist, CharSequence sequence) {
		consORlistORsymb = true;
		if (conslist.equals(CONS))
			addCons();
		else if (conslist.equals(LIST))
			addList();
		else if (conslist.equals(FUNC))
			addFunc(String.valueOf(sequence));
	}

	public void addSymb(CharSequence symb) {
		addSymb(String.valueOf(symb));
	}

	public void addSymb(String symb) {
		if (consORlistORsymb) {
			if (consTmp != null) {
				consTmp.addSymb(symb);
			}
		}
		else {
			if (var1 == null && expr1 == null) {
				var1 = symb;
				symb1 = true;
			}
			else if (var2 == null && expr2 == null) {
				var2 = symb;
				symb2 = true;
			}
			else if (expr1 != null && expr2 == null && var2 == null)
				expr1.addSymb(symb);
			else if (var2 == null) {
				expr2 = new Expression();
				expr2.addSymb(symb);
			}
		}
	}

	public void addVar(String var) {
		if (consORlistORsymb) {
			if (consTmp != null) {
				consTmp.addVar(var);
			}
		}
		else {
			if (var1 == null && expr1 == null)
				var1 = var;
			else if (var2 == null && expr2 == null)
				var2 = var;
			else if (expr1 != null && expr2 == null && var2 == null)
				expr1.addVar(var);
			else if (var2 == null) {
				expr2 = new Expression();
				expr2.addVar(var);
			}
		}
	}

	public void consORlistORsymbEnd() {
		consORlistORsymb = false;
		next = true;
	}

	public void addNil() {
		if (consORlistORsymb) {
			if (consTmp != null) {
				consTmp.addVar(NIL);
			}
		}
		else {
			if (var1 == null && expr1 == null)
				var1 = NIL;
			else if (var2 == null && expr2 == null)
				var2 = NIL;
			else if (expr1 != null && expr2 == null && var2 == null)
				expr1.addVar(NIL);
			else if (expr2 != null && var2 == null)
				expr2.addVar(NIL);
		}
	}

	public void not() {
		not = !not;
	}

	public void addNot() {
		if ((var1 == null && expr1 == null) || (var2 == null && expr2 == null))
			not();
		else if (expr1 != null && expr2 == null && var2 == null)
			expr1.not();
		else if (expr2 != null && var2 == null)
			expr2.not();
	}

	public void addCodeOP(int codeOP) {
		if (var1 == null && expr1 == null) {

		}
		else if (var2 == null && expr2 == null)
			this.codeOP = new CodeOP(codeOP);
		else if (expr1 != null && expr2 == null && var2 == null)
			this.codeOP = new CodeOP(codeOP);
		else if (expr2 != null && var2 == null)
			expr2.codeOP = new CodeOP(codeOP);
	}

	public Expression() {

	}

	public Expression(Expression expression) {
		expr1 = expression;
	}

	public Expression(Expression expr1, Expression expr2, int codeOP) {
		this.expr1 = expr1;
		this.expr2 = expr2;
		this.codeOP = new CodeOP(codeOP);
	}

	public String getVar1() {
		if (symb1) {
			return "\"" + var1 + "\"";
		}
		else {
			if (var1.equals(NIL))
				return "BinTree.NIL";
			if (var1.equals(HD)) {
				hd = true;
				return "";
			}
			if (var1.equals(TL)) {
				tl = true;
				return "";
			}
			if (var1.equals(CONS)) {
				return "BinTree.cons(";
			}
			if (map != null) {
				for (Entry<String, String> e : map.entrySet())
					if (e.getKey().equals(var1))
						return e.getValue();
			}
		}
		return var1;
	}

	public String getVar2() {
		String ret = var2;
		if (symb2) {
			return "" + var2 + "\"";
		}
		else {
			if (var2.equals(NIL))
				return "BinTree.NIL";
			if (map != null) {
				for (Entry<String, String> e : map.entrySet()) {
					if (e.getKey().equals(var2)) {
						if (hd)
							return e.getValue() + ".HD()";
						if (tl)
							return e.getValue() + ".TL()";
						return e.getValue();
					}
				}
			}
			if (hd)
				ret += ".HD()";
			if (tl)
				ret += ".TL()";
		}
		return ret;
	}

	public void addCons() {
		if (consList1 == null && expr1 == null && var1 == null) {
			consList1 = new ConsListFunc(ConsListFunc.CONS);
			consTmp = consList1;
		}
		else if (consList1 != null && expr1 == null && var1 == null) {
			consTmp = consTmp.addCons();

		}
		else if (consList2 == null && expr2 == null && var2 == null) {
			consList2 = new ConsListFunc(ConsListFunc.CONS);
			consTmp = consList2;
		}
		else if (consList2 != null && expr2 == null && var2 == null) {
			consTmp = consTmp.addCons();
		}
	}

	public void addList() {
		if (consList1 == null && expr1 == null && var1 == null) {
			consList1 = new ConsListFunc(ConsListFunc.LIST);
			consTmp = consList1;
		}
		else if (consList1 != null && expr1 == null && var1 == null) {
			consTmp = consTmp.addList();

		}
		else if (consList2 == null && expr2 == null && var2 == null) {
			consList2 = new ConsListFunc(ConsListFunc.LIST);
			consTmp = consList2;
		}
		else if (consList2 != null && expr2 == null && var2 == null) {
			consTmp = consTmp.addList();
		}
	}

	public void addFunc(String func) {
		if (consList1 == null && expr1 == null && var1 == null) {
			consList1 = new ConsListFunc(ConsListFunc.FUNC, func);
			consTmp = consList1;
		}
		else if (consList1 != null && expr1 == null && var1 == null && !next) {
			consTmp = consTmp.addFunc(func);

		}
		else if (consList2 == null && expr2 == null && var2 == null) {
			consList2 = new ConsListFunc(ConsListFunc.FUNC, func);
			consTmp = consList2;
		}
		else if (consList2 != null && expr2 == null && var2 == null) {
			consTmp = consTmp.addFunc(func);
		}
	}

	public String toString() {
		String ret = "";
		if (not) {
			ret += "!";
		}
		if (codeOP != null && codeOP.codeOP == CodeOP.EGAL)
			ret += "(";
		if (consList1 != null) {
			ret += consList1.toString();
		}
		if (var1 != null) {
			ret += getVar1();
		}
		else if (expr1 != null) {
			ret += expr1.toString();
		}
		if (codeOP != null) {
			ret += codeOP.getCodeOP();
		}
		if (consList2 != null) {
			ret += consList2.toString();
		}
		if (var2 != null) {
			ret += getVar2();
		}
		else if (expr2 != null) {
			ret += expr2.toString();
		}
		if (codeOP != null && codeOP.codeOP == CodeOP.EGAL)
			ret += ")";
		return ret;
	}

	public HashMap<String, String> getMap() {
		return map;
	}

	public void setMap(HashMap<String, String> map) {
		this.map = map;
		if (expr1 != null)
			expr1.setMap(map);
		if (expr2 != null)
			expr2.setMap(map);
		if (consList1 != null)
			consList1.setMap(map);
		if (consList2 != null)
			consList2.setMap(map);
	}

}
