package org.xtext.example.mydsl.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.xtext.example.mydsl.beans.SymbFunction;

public class SymbFunction {

	public static final int		FUNCTION	= 0, WHILE = 1, FOR = 2, FOREACH = 3, IF = 4, IFELSE = 5, VAR = 6, AFF = 7, THEN = 8, ELSE = 9;

	public List<String>			inputs;
	public List<String>			outputs;
	public int					type;
	public String				name;
	public String				value;
	private List<SymbFunction>	fonctions;
	public Expression			expr;

	public SymbFunction(String name) {
		this.name = name;
		inputs = new ArrayList<String>();
		outputs = new ArrayList<String>();
		this.type = FUNCTION;
		fonctions = new ArrayList<SymbFunction>();
		this.value = "fun" + (++SymbTable.functionCount);
		SymbTable.functions.put(name, value);
	}

	public SymbFunction(String name, String value) {
		this.name = name;
		this.value = value;
		this.type = VAR;
	}

	public SymbFunction(int type) {
		if (type == FUNCTION) {
			inputs = new ArrayList<String>();
			outputs = new ArrayList<String>();
		}
		this.type = type;
		fonctions = new ArrayList<SymbFunction>();
	}

	public String toStringSymbole() {
		StringBuilder out = new StringBuilder();
		switch (type) {
		case FUNCTION:
			String strInputs = "",
			strOutputs = "";
			for (int i = 0; i < inputs.size(); i++) {
				if (i < inputs.size() - 1)
					strInputs += inputs.get(i) + ", ";
				else
					strInputs += inputs.get(i);
			}
			for (int i = 0; i < outputs.size(); i++) {
				if (i < outputs.size() - 1)
					strOutputs += outputs.get(i) + ", ";
				else
					strOutputs += outputs.get(i);
			}
			out.append("function " + value + " (" + strInputs + "){\n");
			for (SymbFunction s : fonctions) {
				out.append(s.toStringSymbole());
			}
			out.append("\treturn " + "(" + strOutputs + ")\n");
			out.append("}\n");
			break;
		case WHILE:
			break;
		case FOR:
			break;
		case FOREACH:
			break;
		case IF:
			break;
		case IFELSE:
			break;
		case VAR:
			out.append("Var :" + name + " --> " + value + "\n");
			break;
		case AFF:
			out.append(name + " := " + value + "\n");
			break;
		}

		return out.toString();

	}

	public String toString3A() {
		StringBuilder out = new StringBuilder();
		switch (type) {
		case FUNCTION:
			out.append("function " + value + " (){\n");
			for (String s : inputs)
				out.append("<read, _, " + s + ", >\n");
			for (SymbFunction s : fonctions) {
				out.append(s.toString3A());
			}
			for (String s : outputs)
				out.append("<write, _, " + s + ", _>\n");
			out.append("}\n");
			break;
		case WHILE:
			out.append("<while, " + name + ", " + value + ", _>\n{\n");
			for (SymbFunction s : fonctions) {
				out.append(s.toString3A());
			}
			out.append("}\n");
			break;
		case FOR:
			out.append("<FOR, " + name + ", " + value + ", _>\n{\n");
			for (SymbFunction s : fonctions) {
				out.append(s.toString3A());
			}
			out.append("}\n");
			break;
		case FOREACH:
			out.append("<foreach, " + name + ", " + value + ", _>\n{\n");
			for (SymbFunction s : fonctions) {
				out.append(s.toString3A());
			}
			out.append("}\n");
			break;
		case IF:
			out.append("<if, " + name + ", " + value + ", _>\n{\n");
			for (SymbFunction s : fonctions) {
				out.append(s.toString3A());
			}
			out.append("}\n");
			break;
		case IFELSE:
			out.append("<if, " + name + ", " + value + ", _>\n{\n");
			out.append("<then, " + name + ", " + value + ", _>\n{\n");
			for (SymbFunction s : fonctions) {
				if (s.type == THEN) {
					for (SymbFunction s2 : s.fonctions) {
						out.append(s2.toString3A());
					}
				}
			}
			out.append("<else, " + name + ", " + value + ", _>\n{\n");
			for (SymbFunction s : fonctions) {
				if (s.type == ELSE) {
					for (SymbFunction s2 : s.fonctions) {
						out.append(s2.toString3A());
					}
				}
			}
			out.append("}\n");
			break;
		case VAR:
			break;
		case AFF:
			out.append("<aff, " + name + ", " + value + ", _>\n");
			break;
		}

		return out.toString();

	}

	public static int	indent	= 0;

	public String getIndent() {
		String ret = "";
		for (int i = 0; i < indent; i++)
			ret += "	";
		return ret;
	}

	public static HashMap<String, String>	remplaceVar		= new HashMap<String, String>();
	public static int						nbBinTreeCree	= 0;
	public static int						nbListCree		= 0;
	public static int						nbCompteurCree	= 0;

	public String toJAVA() {
		StringBuilder out = new StringBuilder();
		switch (type) {
		case FUNCTION:
			remplaceVar = new HashMap<String, String>();
			nbBinTreeCree = 0;
			nbListCree = 0;
			nbCompteurCree = 0;
			out.append("public static List<BinTree> " + value + "(");
			for (String s : inputs)
				out.append("BinTree " + s + ", ");
			out = new StringBuilder(out.substring(0, out.length() - 2));
			out.append("){\n");
			indent++;
			if (SymbTable.vars.size() > inputs.size()) {
				out.append(getIndent() + "BinTree ");
				boolean foundOne = false;
				for (Entry<String, String> e : SymbTable.vars.entrySet()) {
					boolean found = false;
					for (String s : inputs) {
						if (e.getValue().equals(s)) {
							found = true;
							break;
						}
					}
					if (!found) {
						foundOne = true;
						out.append(e.getValue() + " = null, ");
					}
				}
				if (foundOne)
					out = new StringBuilder(out.substring(0, out.length() - 2) + ";\n");
			}
			for (SymbFunction s : fonctions) {
				out.append(s.toJAVA());
			}
			out.append(getIndent() + "List<BinTree> list = new ArrayList<BinTree>();\n");
			for (String s : outputs)
				out.append(getIndent() + "list.add(" + s + ");\n");
			out.append(getIndent() + "return list;\n");
			out.append("}\n");
			indent--;
			break;
		case WHILE:
			expr.setMap(remplaceVar);
			out.append(getIndent() + "BinTree b" + (++nbBinTreeCree) + " = new BinTree(" + expr + ");\n");
			if (expr.var1 != null && expr.var2 == null && expr.expr2 == null)
				remplaceVar.put(expr.var1, "b" + (nbBinTreeCree));
			expr.setMap(remplaceVar);
			out.append(getIndent() + "while(!b" + nbBinTreeCree + ".isNil()){\n");
			for (SymbFunction s : fonctions) {
				indent++;
				out.append(s.toJAVA());
				indent--;
			}
			out.append(getIndent() + "}\n");
			out.append(getIndent() + expr.var1 +" = b"+nbBinTreeCree+";\n");
			break;
		case FOR:
			expr.setMap(remplaceVar);
			out.append(getIndent() + "BinTree b" + (++nbBinTreeCree) + " = new BinTree(" + expr + ");\n");
			if (expr.var1 != null && expr.var2 == null && expr.expr2 == null)
				remplaceVar.put(expr.var1, "b" + (nbBinTreeCree));
			expr.setMap(remplaceVar);
			out.append(getIndent() + "for(int i" + (++nbCompteurCree) + " = 0 ; i" + nbCompteurCree + " < b" + nbBinTreeCree + ".nbNoeuds() ; i" + nbCompteurCree + "++){\n");
			for (SymbFunction s : fonctions) {
				indent++;
				out.append(s.toJAVA());
				indent--;
			}
			out.append(getIndent() + "}\n");
			break;
		case FOREACH:
			expr.expr1.setMap(remplaceVar);
			expr.expr2.setMap(remplaceVar);
			out.append(getIndent() + "BinTree b" + (++nbBinTreeCree) + " = (" + expr.expr1 + ");\n");
			out.append(getIndent() + "List<BinTree> l" + (++nbListCree) + " = (" + expr.expr2 + ");\n");
			if (expr.expr1.var1 != null && expr.expr1.var2 == null && expr.expr1.expr2 == null)
				remplaceVar.put(expr.expr1.var1, "b" + nbBinTreeCree);
			expr.expr1.setMap(remplaceVar);
			if (expr.expr2.var1 != null && expr.expr2.var2 == null && expr.expr2.expr2 == null)
				remplaceVar.put(expr.expr2.var1, "l" + nbListCree);
			expr.expr2.setMap(remplaceVar);
			out.append(getIndent() + "for(BinTree binTree : l" + nbListCree + "){\n");
			indent++;
			out.append(getIndent() + "while(!binTree.isNil()){\n");
			indent++;
			out.append(getIndent() + "b" + nbBinTreeCree + " = binTree.HD();\n");
			for (SymbFunction s : fonctions) {
				indent++;
				out.append(s.toJAVA());
				indent--;
			}
			out.append(getIndent() + "binTree = binTree.TL();\n");
			indent--;
			out.append(getIndent() + "}\n");
			indent--;
			out.append(getIndent() + "}\n");
			break;
		case IF:
			expr.setMap(remplaceVar);
			out.append(getIndent() + "BinTree b" + (++nbBinTreeCree) + " = new BinTree(" + expr + ");\n");
			if (expr.var1 != null && expr.var2 == null && expr.expr2 == null)
				remplaceVar.put(expr.var1, "b" + (nbBinTreeCree));
			expr.setMap(remplaceVar);
			out.append(getIndent() + "if(b" + nbBinTreeCree + ".isTrue()){\n");
			for (SymbFunction s : fonctions) {
				indent++;
				out.append(s.toJAVA());
				indent--;
			}
			out.append(getIndent() + "}\n");
			break;
		case IFELSE:
			expr.setMap(remplaceVar);
			out.append(getIndent() + "BinTree b" + (++nbBinTreeCree) + " = new BinTree(" + expr + ");\n");
			if (expr.var1 != null && expr.var2 == null && expr.expr2 == null)
				remplaceVar.put(expr.var1, "b" + (nbBinTreeCree));
			expr.setMap(remplaceVar);
			out.append(getIndent() + "if(" + "b" + nbBinTreeCree + ".isTrue()){\n");
			for (SymbFunction s : fonctions) {
				if (s.type == THEN) {
					for (SymbFunction s2 : s.fonctions) {
						indent++;
						out.append(s2.toJAVA());
						indent--;
					}
				}
			}
			out.append(getIndent() + "}else{\n");
			for (SymbFunction s : fonctions) {
				if (s.type == ELSE) {
					for (SymbFunction s2 : s.fonctions) {
						indent++;
						out.append(s2.toJAVA());
						indent--;
					}
				}
			}
			out.append(getIndent() + "}\n");
			break;
		case VAR:
			break;
		case AFF:
			expr.setMap(remplaceVar);
			if (expr.expr2.consList1 != null && expr.expr2.consList1.getType() == ConsListFunc.FUNC) {
				out.append(getIndent() + "List<BinTree> list" + (++nbListCree) + " = BinTree.list(" + expr.expr1 + ");\n");
				out.append(getIndent() + "List<BinTree> list" + (++nbListCree) + " = " + expr.expr2 + ";\n");
				out.append(getIndent() + "for(int i" + (++nbCompteurCree) + " = 0 ; i" + nbCompteurCree + " < list" + (nbListCree - 1) + ".size() ; i" + nbCompteurCree + "++){\n");
				indent++;
				out.append(getIndent() + "list" + (nbListCree - 1) + ".get(i" + nbCompteurCree + ").copie(list" + nbListCree + ".get(i" + nbCompteurCree + "));\n");
				indent--;
				out.append(getIndent() + "}\n");
			}
			else if (expr.expr2.codeOP != null && expr.expr2.codeOP.codeOP == CodeOP.EGAL) {
				out.append(getIndent() + expr.expr1 + " = new BinTree(" + expr.expr2 + ");\n");
			}
			else {
				out.append(getIndent() + expr.expr1 + " = " + expr.expr2 + ";\n");
			}
			break;
		}
		return out.toString();

	}

	public String addVar(CharSequence var) {
		if (SymbTable.vars.containsKey(String.valueOf(var)))
			return SymbTable.vars.get(String.valueOf(var));
		fonctions.add(new SymbFunction(String.valueOf(var), "v" + (++SymbTable.varCount)));
		SymbTable.vars.put(String.valueOf(var), "v" + SymbTable.varCount);
		return "v" + SymbTable.varCount;
	}

	public String getVar(CharSequence var) {
		if (SymbTable.vars.containsKey(String.valueOf(var)))
			return SymbTable.vars.get(String.valueOf(var));
		return String.valueOf(var);
	}

	public void addInputs(CharSequence input) {
		addVar(input);
		inputs.add(getVar(input));
	}

	public void addOutputs(CharSequence output) {
		addVar(output);
		outputs.add(getVar(output));
	}

	public SymbFunction addAssign() {
		SymbFunction symbFunction = new SymbFunction(AFF);
		fonctions.add(symbFunction);
		return symbFunction;
	}

	public SymbFunction addFor() {
		SymbFunction symbFunction = new SymbFunction(FOR);
		fonctions.add(symbFunction);
		return symbFunction;
	}

	public SymbFunction addForeach() {
		SymbFunction symbFunction = new SymbFunction(FOREACH);
		fonctions.add(symbFunction);
		return symbFunction;
	}

	public SymbFunction addIf() {
		SymbFunction symbFunction = new SymbFunction(IF);
		fonctions.add(symbFunction);
		return symbFunction;
	}

	public SymbFunction addIfElse() {
		SymbFunction symbFunction = new SymbFunction(IFELSE);
		SymbFunction symbThen = new SymbFunction(THEN);
		symbFunction.fonctions.add(symbThen);
		SymbFunction symbElse = new SymbFunction(ELSE);
		symbFunction.fonctions.add(symbElse);
		fonctions.add(symbFunction);
		return symbFunction;
	}

	public SymbFunction getThen() {
		for (SymbFunction s : fonctions)
			if (s.type == THEN)
				return s;
		return null;
	}

	public SymbFunction getElse() {
		for (SymbFunction s : fonctions)
			if (s.type == ELSE)
				return s;
		return null;
	}

	public SymbFunction addWhile() {
		SymbFunction symbFunction = new SymbFunction(WHILE);
		fonctions.add(symbFunction);
		return symbFunction;
	}
}