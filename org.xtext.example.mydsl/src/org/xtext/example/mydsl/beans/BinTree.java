package org.xtext.example.mydsl.beans;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BinTree {

	public static final BinTree NIL = new BinTree("NIL");

	private Object noeud;
	private BinTree sag;
	private BinTree sad;

	public BinTree(Object value){
		this.noeud = value;
		sag = null;
		sad = null;
	}

	public BinTree(int value){
		BinTree tmp = generateBinTree(value);
		this.noeud = tmp.getNoeud();
		this.sag = tmp.HD();
		this.sad = tmp.TL();
	}

	public BinTree(Integer value){
		BinTree tmp = generateBinTree(value);
		this.noeud = tmp.getNoeud();
		this.sag = tmp.HD();
		this.sad = tmp.TL();
	}

	public BinTree (BinTree b)
	{
		this.noeud = b.noeud;
		this.sag = b.HD();
		this.sad = b.TL();
	}

	private BinTree(BinTree b1, BinTree b2) {
		this.noeud = "cons (" + b1.getNoeud() + " && " + b2.getNoeud() + ")";
		if(b1.isNil())
		{
			this.sag = b1;
			this.sad = b2;
		}
		else if (b2.isNil())
		{
			this.sag = b2;
			this.sad = b1;
		}
		else
		{
			this.sag = b1.HD();
			this.sad = new BinTree(b1.TL(), b2);
		}
	}

	public static BinTree generateBinTree(int nb)
	{
		if(nb == 0)
			return BinTree.NIL;
		BinTree[] tabBinTree = new BinTree[nb+1];
		for(int i = 0 ; i < nb+1 ; i++)
			tabBinTree[i] = BinTree.NIL;
		return BinTree.cons(tabBinTree);
	}

	public void copie(BinTree b)
	{
		this.noeud = b.getNoeud();
		this.sag = b.HD();
		this.sad = b.TL();
	}

	public boolean isTrue()
	{
		if(this.getNoeud() instanceof Boolean)
			return (Boolean)this.getNoeud();
		return false;
	}

	public int nbNoeuds()
	{
		int compteur = 0;
		BinTree binTree = this;
		while (!binTree.isNil())
		{
			compteur++;
			binTree = binTree.TL();
		}
		return compteur;
	}

	public int nbNoeudsGauche()
	{
		int compteur = 0;
		BinTree binTree = this;
		while (!binTree.isNil())
		{
			compteur++;
			binTree = binTree.HD();
		}
		return compteur;
	}

	public Object getNoeud(){
		return noeud;
	}

	public BinTree HD() {
		return sag;
	}

	public BinTree TL() {
		return sad;
	}

	public boolean isNil(){
		return (sag == null && sad == null);
	}

	public static BinTree cons(BinTree... b){
		if(b.length == 1)
			return new BinTree(b[0], BinTree.NIL);
		else if(b.length == 2)
			return new BinTree(b[0], b[1]);
		else if(subTab(b, 2).length > 1)
			return BinTree.cons(BinTree.cons(b[0], b[1]), BinTree.cons(subTab(b, 2)));
		else
			return BinTree.cons(BinTree.cons(b[0], b[1]), b[2]);

	}

	private static BinTree[] subTab(BinTree[] tab, int from)
	{
		BinTree[] tmpTab = new BinTree[tab.length-from];
		for(int i = 0 ; i < tmpTab.length ; i++)
			tmpTab[i] = tab[i+from];
		return tmpTab;
	}

	public static List<BinTree> list(BinTree...b)
	{
		return Arrays.asList(b);
	}

	public boolean equals(Object o)
	{
		if (o instanceof BinTree)
			return this.getNoeud().equals(((BinTree) o).getNoeud());
		return false;
	}

	public String toString()
	{
		String res = "";
				if(this.isNil())
					res += drawTree(this, 0, 0, 0, 0);
				else
					res += drawTree(this, 0, 3, 0, this.HD().getNoeud().toString().length());

		//res += drawTree(this, this.nbNoeudsGauche(), this.nbNoeuds());

		return res;
	}

//	public String drawTree(BinTree b, int nbNoeudGauche, int nbNoeudDroite)
//	{
//		String res = "";
//		int nbNoeudMax = 0;
//		int nbSpace = 0;
//		int nbSpaceADroite = 0;
//		if(nbNoeudGauche > nbNoeudDroite)
//			nbNoeudMax = nbNoeudGauche;
//		else
//			nbNoeudMax = nbNoeudDroite;
//		int i;
//
//		for(i = 0 ; i < nbNoeudGauche ; i++)
//			for(int j = 0 ; j < (nbNoeudMax-i) ; j++)
//				nbSpace++;
//		if(b.isNil())
//		{
//			for(i = 0 ; i < nbSpace ; i++)
//				res += " ";
//			res += b.getNoeud().toString();
//		}
//		else
//		{
//			for(i = 0 ; i < nbSpace ; i++)
//				res += " ";
//			res += b.getNoeud().toString() + "\n";
//
//			for(i = 0 ; i < nbNoeudMax ; i++)
//			{
//				for(int j = 0 ; j < (nbSpace - i) ; j++)
//					res += " ";
//
//				res += "/";
//
//				for(int k = 0 ; k < (nbNoeudMax+2+i) ; k++)
//				{
//					res += " ";
//					nbSpaceADroite++;
//				}
//				res += "\\\n";
//			}
//
//			String s = "";
//			for(i = 0 ; i < nbSpaceADroite ; i++)
//				s += " ";
//			res += drawTree(b.HD(),b.HD().nbNoeudsGauche(), b.HD().nbNoeuds()) + s + drawTree(b.TL(),b.TL().nbNoeudsGauche(), b.TL().nbNoeuds());
//
//
//		}
//
//		return res;
//	}

	public String drawTree(BinTree b, int nbLigne, int nbSpace, int type, int nbLettreSag)
	{
		String res = "";
		int i;

		if(b.isNil())
		{
			if(type == 1)
			{
				res += "\n";
				for(i = 0 ; i < nbSpace ;i++)
					res += " ";
			}
			else if (type == 2)
			{
				for(i = 0 ; i < (6-nbLettreSag)+1 ;i++)
					res += " ";
			}
			res += b.getNoeud();
		}
		else
		{
			for(i = 0 ; i < (6-nbLettreSag)+1 ;i++)
				res += " ";
			res += b.getNoeud();
			for(i = 0 ; i < nbLigne+1 ;i++)
				res += "\n";
			for(i = 0 ; i < nbSpace-1 ;i++)
				res += " ";
			res += "/" + "   " + "\\";

			for(i = 0 ; i < nbLigne+1 ;i++)
				res += "\n";
			for(i = 0 ; i < nbSpace-2 ;i++)
				res += " ";
			res += "/" + "     " + "\\";

			res += drawTree(b.HD(), nbLigne+3, nbSpace-3, 1, 0);
			res += drawTree(b.TL(), nbLigne, nbSpace+3, 2, b.HD().getNoeud().toString().length());
		}
		return res;
	}

	public static String toStringListBinTree(List<BinTree> list)
	{
		String res = "";
		for(BinTree b : list)
		{
			res+=b.toString();
			res+="\n\n==============================================================================================\n\n";
		}
		return res;
	}

	public static void main (String[] args)
	{
		BinTree b1 = BinTree.cons(BinTree.cons(BinTree.cons(BinTree.cons(BinTree.NIL))), BinTree.cons(BinTree.cons(BinTree.cons(BinTree.cons(BinTree.NIL)))));
		BinTree b2 = BinTree.cons(BinTree.cons(BinTree.cons(BinTree.NIL)), BinTree.cons(BinTree.cons(BinTree.NIL)));
		BinTree b3 = BinTree.cons(BinTree.NIL,BinTree.NIL,BinTree.NIL);
		BinTree b4 = BinTree.cons(BinTree.cons(BinTree.NIL));
		BinTree b5 = BinTree.NIL;
		System.out.println(b4);
		System.out.println(b4.nbNoeuds());
		System.out.println(generateBinTree(6));
		//		List<BinTree> list = new ArrayList<BinTree>();
		//		list.add(b1);
		//		list.add(b2);
		//		list.add(b3);
		//		list.add(b4);
		//		list.add(b5);
		//		BinTree[] tab = list.toArray(new BinTree[list.size()]);
		//		BinTree b6 = BinTree.cons(tab);
		//		System.out.println(b6);
		//		System.out.println(BinTree.toStringListBinTree(list));

	}
}
