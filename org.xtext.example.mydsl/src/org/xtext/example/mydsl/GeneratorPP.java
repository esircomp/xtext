package org.xtext.example.mydsl;

import java.util.HashMap;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.xtext.generator.JavaIoFileSystemAccess;
import org.xtext.example.mydsl.beans.SymbTable;
import org.xtext.example.mydsl.generator.MyDslGenerator;
import org.xtext.example.mydsl.generator.MyDslGenerator;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;

public class GeneratorPP {
	@Inject
	private Provider<ResourceSet>	resourceSetProvider;

	@Inject
	private MyDslGenerator		generator;

	@Inject
	private JavaIoFileSystemAccess	fileAccess;

	public SymbTable				symbTable;
	private GeneratorPP				generatorPP;

	public void runGenerator(String inputFile, String outputFile, HashMap<String, String> map, boolean generateSymbTable) {
		// load the resource
		ResourceSet set = resourceSetProvider.get();
		Resource resource = set.getResource(URI.createFileURI(inputFile), true);
		fileAccess.setOutputPath("./outputs/");
		symbTable = new SymbTable();
		generator.generate(resource, fileAccess, outputFile, map, symbTable, generateSymbTable);
	}

	public void runGenerator(String inputFile, String outpuFolder, String outputFile, HashMap<String, String> map, boolean generateSymbTable) {
		// load the resource
		ResourceSet set = resourceSetProvider.get();
		Resource resource = set.getResource(URI.createFileURI(inputFile), true);
		fileAccess.setOutputPath(outpuFolder);
		symbTable = new SymbTable();
		generator.generate(resource, fileAccess, outputFile, map, symbTable, generateSymbTable);

	}

	public void generate(String inputFile, String outputFile, HashMap<String, String> map, boolean generateSymbTable) {
		Injector injector = new MyDslStandaloneSetup().createInjectorAndDoEMFRegistration();
		generatorPP = injector.getInstance(GeneratorPP.class);
		generatorPP.runGenerator(inputFile, outputFile, map, generateSymbTable);

	}

	public void generate(String inputFile, String outfolder, String outputFile, HashMap<String, String> map, boolean generateSymbTable) {
		Injector injector = new MyDslStandaloneSetup().createInjectorAndDoEMFRegistration();
		generatorPP = injector.getInstance(GeneratorPP.class);
		generatorPP.runGenerator(inputFile, outfolder, outputFile, map, generateSymbTable);

	}

	public SymbTable getSymTable() {
		return generatorPP.symbTable;
	}

}
