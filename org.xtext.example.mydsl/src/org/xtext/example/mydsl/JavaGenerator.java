package org.xtext.example.mydsl;

import java.io.File;

public class JavaGenerator {
	public static void main(String[] args) {

		// args = new String[]{"C:/Users/Thomas/git/xtext/Tests/test1.wh",
		// "-symbole"};

		args = new String[] { "/Users/DUARTE/Documents/workspaceXTEXT/xTextProjet/Tests/testsPresentation/test5.wh"};
		//args = new String[] { "/Users/DUARTE/Documents/workspaceXTEXT/xTextProjet/org.xtext.example.mydsl.tests/testCompilateursSimple/test_symb1.wh"};
		//args = new String[] { "C:/Users/sylva/workspaceXText/xtext/Tests/test_foreach.wh"};

		if (args.length == 0) {
			System.out.println("Il faut préciser le fichier d'entré");
			System.out.println("-help pour plus d'informations");
			return;
		}
		String inputFile = args[0];

		File file = new File(inputFile);
		if (!file.exists()) {
			System.out.println("Ce fichier n'existe pas");
			return;
		}

		GeneratorSym generatorSym = new GeneratorSym();
		generatorSym.generate(inputFile);
		generatorSym.getSymTable().displayJAVA();

	}
}
